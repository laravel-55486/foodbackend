<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('storeName');
            $table->string('businessName');
            $table->string('header_img')->default('header.png');
            $table->string('bg_img')->default('bg.png');
            $table->string('logo_img')->default('logo.png');
            $table->string('phone');
            $table->string('street');
            $table->string('city');
            $table->string('postalZip');
            $table->string('country');
            $table->string('country_code');
            $table->string('currency');
            $table->decimal('deliveryCost');
            $table->string('monday');
            $table->string('tuesday');
            $table->string('wednesday');
            $table->string('thursday');
            $table->string('friday');
            $table->string('saturday');
            $table->string('sunday');
            $table->string('longitude');
            $table->string('latitude');
            $table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_infos');
    }
}
