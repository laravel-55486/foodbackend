<?php

use App\StoreInfo;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class StoreInfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('store_infos')->delete();

        $stores = [
           [
               'storeName' => "John Doe's restaurant",
               'businessName' => 'John DoeldlyDoe',
               'phone' => 687961984,
               'street' => 'Zuidvliet 111',
               'city' => 'Maassluis',
               'postalZip' => '3141AP',
               'country' => 'Netherlands',
               'country_code' => 'NL',
               'currency' => 'EUR',
               'deliveryCost' => 1.50,
               'monday' => '10:00 - 22:30',
               'tuesday' => '10:00 - 22:30',
               'wednesday' => '10:00 - 22:30',
               'thursday' => '10:00 - 22:30',
               'friday' => '10:00 - 23:30',
               'saturday' => '12:00 - 01:30',
               'sunday' => '12:00 - 18:00',
               'longitude' => "4.47773",
               'latitude' => "51.9244",
               'message' => '<span>Keep in mind!</span><br />We do not deliver to customers who do not provide any details</p>',
           ],
        ];

        foreach ($stores as $store) {
           StoreInfo::create($store);
        }
        Model::reguard();
    }
}
