@extends('layouts.app')
@section('title')
    Admin Profile
@endsection
@section('page_style')
    <!--page level css -->
    <link href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/vendors/x-editable/css/bootstrap-editable.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/slim/slim.min.css')}}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{asset('assets/css/pages/advmodals.css')}}" rel="stylesheet" /> --}}
    <link href="{{asset('assets/css/pages/user_profile.css')}}" rel="stylesheet" type="text/css" />
    <!--end of page level css-->
@endsection
@section('welcome_text')
    <h1>Admin Profile</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard')}}">
                <i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard
            </a>
        </li>
        <li class="active">Admin Profile</li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav  nav-tabs ">
                <li class="active">
                    <a href="#tab1" data-toggle="tab">
                        <i class="livicon" data-name="user" data-size="16" data-c="#000" data-hc="#000" data-loop="true"></i> My Profile</a>
                </li>
                <li>
                    <a href="#tab2" data-toggle="tab">
                        <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i> Change Password</a>
                </li>
            </ul>
            <div class="tab-content mar-top">
                <div id="tab1" class="tab-pane fade active in">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        Admin Profile
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-4">
                                        <div class="text-center">
                                            <img style="max-width: 180px; max-height:180px;background-color: #fff;border: 1px solid #ddd;padding:4px;border-radius: 4px;" src="{{asset('avatars/admin/'.Auth::user()->avatar)}}" alt="">
                                            <br />
                                            <button class="btn btn-responsive button-alignment btn-info" data-toggle="modal" data-target="#avatar_modal" style="margin: 10px 0">Select Image</button>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped" id="users">
                                                    <tr>
                                                        <td>First Name</td>
                                                        <td>
                                                            <a href="#" class="editable" data-type="text" data-pk="1" data-name="first_name" data-url="{{route('profile.ud.bio')}}" data-original-title="Edit First Name">{{Auth::user()->first_name}}</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Last Name</td>
                                                        <td>
                                                            <a href="#" class="editable" data-type="text" data-pk="1" data-name="last_name" data-url="{{route('profile.ud.bio')}}" data-original-title="Edit Last Name">{{Auth::user()->last_name}}</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>E-mail</td>
                                                        <td>
                                                            <a href="#" class="editable" data-type="text" data-pk="1" data-name="email" data-url="{{route('profile.ud.bio')}}" data-original-title="Edit Email">{{Auth::user()->email}}</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab2" class="tab-pane fade">
                    <div class="row">
                        <div class="col-md-12 pd-top">
                            <form action="{{route('profile.ud.pass')}}" id="admin_password_change_form" method="post" class="form-horizontal">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label for="inputpassword" class="col-md-3 control-label">
                                            Password
                                            <span class='require'>*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                                </span>
                                                <input type="password" name="new_password" id="inputpassword" placeholder="Password" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-6 padding">
                                                    <span id="6char" class="glyphicon glyphicon-remove color-pwd"></span> 6 Characters Long
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputnumber" class="col-md-3 control-label">
                                            Confirm Password
                                            <span class='require'>*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                                </span>
                                                <input type="password" id="inputnumber" placeholder="Confirm Password" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12 padding">
                                                    <span id="pwmatch" class="glyphicon glyphicon-remove color-pwd"></span> Passwords Match
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="form-group">
                                        <div class="col-md-9 col-md-offset-3">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="avatar_modal" role="dialog" aria-labelledby="modalLabelfade" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade">Choose Avatar</h4>
                </div>
                <form id="m-user__pic-cover-upload-form" action="{{route('profile.ud.avatar')}}" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div id="m-admin__pic-cover-slim">
                            <input type="file" name="slim[]" required/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('custom_script')
    <script type="text/javascript">
        var admin_avatar_cropper = new Slim(document.getElementById('m-admin__pic-cover-slim'), {
            ratio: '1:1',
            minSize: {
                width: 150,
                height: 150,
            },
            download: false,
            label: 'Drop your image here or Click.',
            statusImageTooSmall:'Image too small. Min Size is $0 pixel. Try again',
        });
        admin_avatar_cropper.size = { width:700, height:700 };
    </script>
@endsection
@section('page_script')
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/vendors/jquery-mockjax/js/jquery.mockjax.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/vendors/x-editable/js/bootstrap-editable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/slim/slim.kickstart.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/pages/user_profile.js')}}" type="text/javascript"></script>
    <!-- end of page level js -->
@endsection
