@extends('layouts.app')
@section('title')
    Dashboard
@endsection
@section('page_style')
    <link rel="stylesheet" href="{{asset('assets/vendors/Buttons/css/buttons.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/buttons.bootstrap.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/pages/advbuttons.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/pages/tab.css')}}" />
    {{-- <link rel="stylesheet" href="{{asset('assets/css/pages/only_dashboard.css')}}" /> --}}
    <!--end of page level css-->
@endsection
@section('custom_style')
    <style media="screen">
        .dashboard-tap-btn.active {
            background-color: #005ca0;
            border-color: unset;
            color: #fff;
            box-shadow: unset;
        }
        .chart-container {
            height: 600px;
        }
        .chart-loading-view {
            width: 100%;
            height: 100%;
            position: absolute;
            top:0;
            left:0;
            z-index: 1;
            background-color: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
        }
    </style>
@endsection
@section('welcome_text')
    <h1>Welcome to Dashboard</h1>
    <ol class="breadcrumb">
        <li class="active">
            <a href="javascript:void(0)">
                <i class="livicon" data-name="home" data-size="14" data-color="#333" data-hovercolor="#333"></i> Dashboard
            </a>
        </li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInDown">
            <!-- Trans label pie charts strats here-->
            <div class="lightbluebg no-radius">
                <div class="panel-body squarebox square_boxs">
                    <div class="col-xs-12 pull-left nopadmar">
                        <div class="row">
                            <div class="square_box col-xs-7 text-right">
                                <span>Views Today</span>
                                <div class="number" id="user_view_day" data-value={{$user_count->day}} ></div>
                            </div>
                            <i class="livicon  pull-right" data-name="eye-open" data-l="true" data-c="#fff" data-hc="#fff" data-s="70"></i>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <small class="stat-label">This Week</small>
                                <h4 id="user_view_week" data-value={{$user_count->week}}></h4>
                            </div>
                            <div class="col-xs-6 text-right">
                                <small class="stat-label">This Month</small>
                                <h4 id="user_view_month" data-value={{$user_count->month}} ></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-md-6 margin_10 animated fadeInDown">
            <!-- Trans label pie charts strats here-->
            <div class="goldbg no-radius">
                <div class="panel-body squarebox square_boxs">
                    <div class="col-xs-12 pull-left nopadmar">
                        <div class="row">
                            <div class="square_box col-xs-7 pull-left">
                                <span>Orders Today</span>
                                <div class="number" id="user_order_day" data-value={{$order_count->day}}></div>
                            </div>
                            <i class="livicon pull-right" data-name="pen" data-l="true" data-c="#fff" data-hc="#fff" data-s="70"></i>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <small class="stat-label">This Week</small>
                                <h4 id="user_order_week" data-value={{$order_count->week}}></h4>
                            </div>
                            <div class="col-xs-6 text-right">
                                <small class="stat-label">This Month</small>
                                <h4 id="user_order_month" data-value={{$order_count->month}}></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInDown">
            <!-- Trans label pie charts strats here-->
            <div class="redbg no-radius">
                <div class="panel-body squarebox square_boxs">
                    <div class="col-xs-12 pull-left nopadmar">
                        <div class="row">
                            <div class="square_box col-xs-7 pull-left">
                                <span>Cash Flow Today</span>
                                <div class="number" id="user_cash_day" data-value={{$cash_count->day}}></div>
                            </div>
                            <i class="livicon pull-right" data-name="piggybank" data-l="true" data-c="#fff" data-hc="#fff" data-s="70"></i>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <small class="stat-label">This Week</small>
                                <h4 id="user_cash_week" data-value={{$cash_count->week}}></h4>
                            </div>
                            <div class="col-xs-6 text-right">
                                <small class="stat-label">This Month</small>
                                <h4 id="user_cash_month" data-value={{$cash_count->month}}></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInDown">
            <!-- Trans label pie charts strats here-->
            <div class="palebluecolorbg no-radius">
                <div class="panel-body squarebox square_boxs">
                    <div class="col-xs-12 pull-left nopadmar">
                        <div class="row">
                            <div class="square_box col-xs-7 pull-left">
                                <span>Registered Users</span>
                                <div class="number" id="user_register_day" data-value={{$register_count->day}}></div>
                            </div>
                            <i class="livicon pull-right" data-name="users" data-l="true" data-c="#fff" data-hc="#fff" data-s="70"></i>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <small class="stat-label">This Week</small>
                                <h4 id="user_register_week" data-value={{$register_count->week}}></h4>
                            </div>
                            <div class="col-xs-6 text-right">
                                <small class="stat-label">This Month</small>
                                <h4 id="user_register_month" data-value={{$register_count->month}}></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/row-->
    <div class="row ">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left add_remove_title">
                        <i class="livicon" data-name="piggybank" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Sales Chart
                    </h3>
                    <div class="pull-right">
                        <div class="button-group">
                            <button type="button" data-target="#dashboard_sales_day" class="button button-primary dashboard-tap-btn active" data-toggle="tab">Day</button>
                            <button type="button" data-target="#dashboard_sales_week" class="button button-primary dashboard-tap-btn" data-toggle="tab">Week</button>
                            <button type="button" data-target="#dashboard_sales_month" class="button button-primary dashboard-tap-btn" data-toggle="tab">Month</button>
                            <button type="button" data-target="#dashboard_sales_year" class="button button-primary dashboard-tap-btn" data-toggle="tab">Year</button>
                        </div>
                    </div>
                </div>
                <div class="panel-body" style="position: relative;">
                    <div class="chart-loading-view" id="chart-loading-view">
                        <i class="livicon" data-name="spinner-seven" data-size="80" data-loop="true" data-c="#246eb3" data-hc="white"></i>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active in fade" id="dashboard_sales_day">
                            <div class="chart-container" id="day_chart"></div>
                        </div>
                        <div class="tab-pane fade" id="dashboard_sales_week">
                            <div class="chart-container" id="week_chart"></div>
                        </div>
                        <div class="tab-pane fade" id="dashboard_sales_month">
                            <div class="chart-container" id="month_chart"></div>
                        </div>
                        <div class="tab-pane fade" id="dashboard_sales_year">
                            <div class="chart-container" id="year_chart"></div>
                        </div>
                    </div>
                    <!--tab ends-->
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
@endsection
@section('custom_script')
    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawDay);
        google.charts.setOnLoadCallback(drawWeek);
        google.charts.setOnLoadCallback(drawMonth);
        google.charts.setOnLoadCallback(drawYear);

        function drawDay() {

            var data = google.visualization.arrayToDataTable([
                ["Hour", "Sale", { role: "style" } ],
                ["0:00", 10, "#00bc8c"],
                ["1:00", 40, "#00bc8c"],
                ["2:00", 20, "#00bc8c"],
                ["3:00", 70, "#00bc8c"],
                ["4:00", 40, "#00bc8c"],
                ["5:00", 80, "#00bc8c"],
                ["6:00", 90, "#00bc8c"],
                ["7:00", 270, "#00bc8c"],
                ["8:00", 230, "#00bc8c"],
                ["9:00", 220, "#00bc8c"],
                ["10:00", 240, "#00bc8c"],
                ["11:00", 280, "#00bc8c"],
                ["12:00", 200, "#00bc8c"],
                ["13:00", 210, "#00bc8c"],
                ["14:00", 220, "#00bc8c"],
                ["15:00", 230, "#00bc8c"],
                ["16:00", 250, "#00bc8c"],
                ["17:00", 260, "#00bc8c"],
                ["18:00", 230, "#00bc8c"],
                ["19:00", 270, "#00bc8c"],
                ["20:00", 220, "#00bc8c"],
                ["21:00", 290, "#00bc8c"],
                ["22:00", 120, "#00bc8c"],
                ["23:00", 20, "#00bc8c"],
            ]);

            var view = new google.visualization.DataView(data);

            var options = {
                title: 'Sales of Day',
                hAxis: {
                    title: 'Time of Day',
                    format: 'h:mm a'
                },
                vAxis: {
                    title: 'Sales/Euros',
                    locale: 'pt_BR',
                    format: '€ '
                },
                chartArea: {
                    left: "5%",
                    top: "10%",
                    height: "75%",
                    width: "95%"
                },
                legend: { position: "none" },
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('day_chart'));

            chart.draw(view, options);
        }

        function drawWeek() {

            var data = google.visualization.arrayToDataTable([
                ["Week", "Sale", { role: "style" } ],
                ["Monday", 3600, "#00bc8c"],
                ["Tuseday", 4000, "#00bc8c"],
                ["Wednesday", 2700, "#00bc8c"],
                ["Thursday", 4100, "#00bc8c"],
                ["Friday", 2300, "#00bc8c"],
                ["Saturday", 3800, "#00bc8c"],
                ["Sunday", 4900, "#00bc8c"],
            ]);

            var view = new google.visualization.DataView(data);

            var options = {
                title: 'Sales of Week',
                hAxis: {
                    title: 'Day of Week',
                },
                vAxis: {
                    title: 'Sales/Euros',
                    locale: 'pt_BR',
                    format: '€ '
                },
                chartArea: {
                    left: "5%",
                    top: "10%",
                    height: "75%",
                    width: "95%"
                },
                legend: { position: "none" },
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('week_chart'));

            chart.draw(view, options);
        }

        function drawMonth() {

            var data = google.visualization.arrayToDataTable([
                ["Week", "Sale", { role: "style" } ],
                ["1 Week", 14300, "#00bc8c"],
                ["2 Week", 15400, "#00bc8c"],
                ["3 Week", 21000, "#00bc8c"],
                ["4 Week", 17000, "#00bc8c"],
                ["5 Week", 14700, "#00bc8c"],
            ]);

            var view = new google.visualization.DataView(data);

            var options = {
                title: 'Sales of Month',
                hAxis: {
                    title: 'Week of Month',
                },
                vAxis: {
                    title: 'Sales/Euros',
                    locale: 'pt_BR',
                    format: '€ '
                },
                chartArea: {
                    left: "5%",
                    top: "10%",
                    height: "75%",
                    width: "95%"
                },
                legend: { position: "none" },
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('month_chart'));

            chart.draw(view, options);
        }

        function drawYear() {

            var data = google.visualization.arrayToDataTable([
                ["Month", "Sale", { role: "style" } ],
                ["Jan", 82000, "#00bc8c"],
                ["Feb", 76000, "#00bc8c"],
                ["Mar", 69400, "#00bc8c"],
                ["Apr", 87000, "#00bc8c"],
                ["May", 47000, "#00bc8c"],
                ["June", 94000, "#00bc8c"],
                ["July", 85000, "#00bc8c"],
                ["Aug", 67000, "#00bc8c"],
                ["Sep", 79000, "#00bc8c"],
                ["Oct", 105000, "#00bc8c"],
                ["Nov", 92000, "#00bc8c"],
                ["Dec", 59000, "#00bc8c"],
            ]);

            var view = new google.visualization.DataView(data);

            var options = {
                title: 'Sales of Year',
                hAxis: {
                    title: 'Month of Year',
                },
                vAxis: {
                    title: 'Sales/Euros',
                    locale: 'pt_BR',
                    format: '€ '
                },
                chartArea: {
                    left: "5%",
                    top: "10%",
                    height: "75%",
                    width: "95%"
                },
                legend: { position: "none" },
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('year_chart'));

            chart.draw(view, options);
        }

        $(document).ready(function () {
            $(window).resize(function(){
                drawDay();
                drawWeek();
                drawMonth();
                drawYear();
            });
        });
        window.onload = function () {
			setTimeout(function () {
				$('#chart-loading-view').fadeOut('slow');
			}, 500);
		}
    </script>
@endsection
@section('page_script')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="{{asset('assets/vendors/countUp.js/js/countUp.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/vendors/Buttons/js/buttons.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/dashboard.js')}}" type="text/javascript"></script>
@endsection
