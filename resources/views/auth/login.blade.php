<!DOCTYPE html>
<html>

<head>
    <title>Login | Restaurant Admin panel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- global level css -->
    <link href="{{asset('/assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <!-- end of global level css -->
    <link href="{{asset('/assets/vendors/iCheck/css/square/blue.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}" rel="stylesheet" />
    <!-- page level css -->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/css/pages/login.css')}}" />
    <!-- end of page level css -->
</head>

<body>
    <div class="container">
        <div class="row vertical-offset-100">
            <div class="col-sm-6 col-sm-offset-3  col-md-5 col-md-offset-4 col-lg-4 col-lg-offset-4">
                <div id="container_demo">
                    <div id="wrapper">
                        <div id="login" class="animate form">
                            <form action="{{route('login')}}" autocomplete="on" method="post">
                                {{ csrf_field() }}
                                <h3 class="black_bg">
                                    <img src="{{asset('assets/img/logo.png')}}" alt="josh logo">
                                    <br>Log In</h3>
                                @if (session('error'))
                                    <div class="alert alert-danger alert-dismissable margin5">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        {{ session('error') }}
                                    </div>
                                @endif
                                <div class="form-group ">
                                    <label style="margin-bottom:0;" for="email1" class="uname control-label">
                                        <i class="livicon" data-name="mail" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i> E-mail
                                    </label>
                                    <input id="email1" name="email" placeholder="E-mail" value="" />
                                    <div class="col-sm-12"></div>
                                </div>
                                <div class="form-group ">
                                    <label style="margin-bottom:0;" for="password" class="youpasswd">
                                        <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i> Password
                                    </label>
                                    <input type="password" id="password" name="password" placeholder="Enter a password" />
                                    <div class="col-sm-12"></div>
                                </div>
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="remember" id="remember-me" value="remember-me" class="square-blue" /> Keep me logged in
                                    </label>
                                </div>
                                <p class="login button">
                                    <input type="submit" value="Log In" class="btn btn-success" />
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- global js -->
    <script src="{{asset('/assets/js/app.js')}}" type="text/javascript"></script>
    <!-- end of global js -->
    <script src="{{asset('/assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/js/pages/login.js')}}" type="text/javascript"></script>
</body>

</html>
