@extends('layouts.app')
@section('title')
    Category Products
@endsection
@section('page_style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/dataTables.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/buttons.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/rowReorder.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/sweetalert/css/sweetalert.css')}}" />
    <link href="{{asset('/assets/vendors/iCheck/css/square/blue.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/vendors/Buttons/css/buttons.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/pages/advbuttons.css')}}" />
    <link href="{{asset('assets/vendors/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/vendors/select2/css/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/plugins/slim/slim.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/tables.css')}}" />
@endsection
@section('custom_style')
    <link href="{{asset('assets/css/pages/sortable_list.css')}}" rel="stylesheet" type="text/css">
    <style media="screen">
        .select2-search.select2-search--dropdown {
            display: none;
        }
        td {
            vertical-align: middle!important;
        }
    </style>
@endsection
@section('welcome_text')
    <h1>Category Products</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard')}}">
                <i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{route('category')}}">
                <i class="livicon" data-name="tags" data-size="14" data-loop="true"></i> Category
            </a>
        </li>
        <li class="active">Category/product</li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('category')}}" class="button button-rounded button-primary button-small">Back</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable" style="overflow:auto;">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="tags" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Category - {{$category->categoryName}}
                    </h3>
                </div>
                <div class="panel-body table-responsive">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>position</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$category->id}}</td>
                                    <td style="padding: 0;width:250px;">
                                        <div class="">
                                            <img style="width:100%;max-width:300px;" src="{{asset('uploads/categories/'.$category->image)}}" alt="">
                                        </div>
                                    </td>
                                    <td>{{$category->categoryName}}</td>
                                    <td>{{$category->categoryDescription}}</td>
                                    <td align="center">{{$category->position}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left add_remove_title">
                        <i class="livicon" data-name="beer" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Products
                    </h3>
                    <div class="pull-right">
                        <a type="button" data-toggle="modal" data-target="#category_product_sort_modal" class="button button-rounded button-primary button-small">Change Sort</a>
                        <a type="button" data-toggle="modal" data-target="#add-products-modal" class="button button-rounded button-success button-small">Add Product</a>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table table-striped table-bordered" id="category_product_table">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>Position</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cat_products as $cat_product)
                                <tr>
                                    <td>
                                        <div class="">
                                            <img style="width:100%;max-width: 290px;" src="{{asset('uploads/products/'.$cat_product->cover_image)}}" alt="">
                                        </div>
                                    </td>
                                    <td>{{$cat_product->title}}</td>
                                    <td>{{$cat_product->description}}</td>
                                    <td align="center">{{$cat_product->price}} €</td>
                                    <td align="center">{{$cat_product->position}}</td>
                                    <td>
                                        <div class="">
                                            <button type="button" data-cat_product_id="{{$cat_product->id}}" class="cat_product_delete_btn btn btn-responsive button-alignment btn-danger">Delete</button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="add-products-modal" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action="{{route('category.add.products')}}" id="cat_add_product_form" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="category_id_product_add" value="{{$category->id}}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Add product to this category</h4>
                    </div>
                    <div class="modal-body table-responsive">
                        <table class="table table-striped table-bordered" id="add_product_table">
                            <thead>
                                <tr>
                                    <th style="text-align:center;">#</th>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td style="text-align:center;">
                                            <input type="checkbox" name="selected_products[]" value="{{$product->id}}" class="square-blue" />
                                        </td>
                                        <td align="center" >{{$product->id}}</td>
                                        <td>{{$product->title}}</td>
                                        <td>{{$product->description}}</td>
                                        <td align="center">{{$product->price}} €</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="button-small button button-rounded button-danger">Close</button>
                        <button type="submit" class="button-small button button-rounded button-primary">Add Selected Products</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="category_product_sort_modal" role="dialog" aria-labelledby="modalLabelfade2" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade2">Change Order</h4>
                </div>
                <form id="category_product_sort_form" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div data-force="30" class="layer block">
                            <ul id="foo" class="block__list block__list_words">
                                @foreach ($cat_products as $cat_product)
                                    <li data-id="{{$cat_product->id}}">{{$cat_product->title}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Save Order
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('custom_script')
    <script type="text/javascript">
        var local_cat_product_sortable;
        var order_init = {{json_encode($orderlist)}};
        localStorage.setItem(local_cat_product_sortable, order_init.join('|'));
    </script>
    <script src="{{asset('js/admin_cat_product_sort.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('js/admin_cat_product.js')}}"></script>
@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jeditable/js/jquery.jeditable.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.rowReorder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/sweetalert/js/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendors/Sortable/js/Sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/pdfmake.js')}}"></script>
    <script src="{{asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/slim/slim.kickstart.min.js')}}" type="text/javascript"></script>
@endsection
