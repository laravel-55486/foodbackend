@extends('layouts.app')
@section('title')
    Push
@endsection
@section('page_style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/dataTables.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/buttons.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/rowReorder.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/sweetalert/css/sweetalert.css')}}" />
    <link href="{{asset('assets/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/vendors/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/vendors/select2/css/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/plugins/slim/slim.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/tables.css')}}" />
@endsection
@section('custom_style')
    <style media="screen">
        .select2-search.select2-search--dropdown {
            display: none;
        }
        #extra_table td {
            vertical-align: middle;
        }
    </style>
@endsection
@section('welcome_text')
    <h1>Notifications</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard')}}">
                <i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard
            </a>
        </li>
        <li class="active">push</li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <button type="button" data-toggle="modal" data-target="#new_cupon_modal" class="pull-right btn btn-responsive button-alignment btn-info">New Notification</button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable" style="overflow:auto;">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="bell" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Notification History
                    </h3>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table table-striped table-bordered" id="push_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>User Name</th>
                                <th>Notify Title</th>
                                <th>Notify Body</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($notifications as $notification)
                                <tr>
                                    <td align="center" >{{$notification->id}}</td>
                                    <td>{{$notification->name}}</td>
                                    <td>{{$notification->msg_title}}</td>
                                    <td>{{$notification->msg_text}}</td>
                                    <td>{{$notification->created_at}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="new_cupon_modal" role="dialog" aria-labelledby="modalLabelfade" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade">Send New Push</h4>
                </div>
                <form id="new_push_send_form" action="{{route('push.notify.send')}}" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label" for="notify_title">Notification Title:</label>
                            <input type="text" class="form-control" name="notify_title" placeholder="Title" id="notify_title" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="notify_text">Notification Body:</label>
                            <input type="text" id="notify_text" name="notify_text" placeholder="Text" class="form-control cupon_percent_box" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="cupon_user">Cupon User</label>
                            <div class="modal-body table-responsive">
                                <table class="table table-striped table-bordered" id="notify_user_table">
                                    <thead>
                                        <tr>
                                            <th class="table-checkbox" style="text-align:center;">
                                                <input type="checkbox" class="group-checkable" data-set="#notify_user_table .checkboxes" />
                                            </th>
                                            <th>ID</th>
                                            <th>User</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $user)
                                            <tr>
                                                <td style="text-align:center;">
                                                    <input type="checkbox" name="selected_users[]" value="{{$user->id}}"  class="checkboxes" />
                                                </td>
                                                <td align="center" >{{$user->id}}</td>
                                                <td>{{$user->email}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('custom_script')
    <script type="text/javascript" src="{{asset('js/admin_push.js')}}"></script>
@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jeditable/js/jquery.jeditable.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.rowReorder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/sweetalert/js/sweetalert.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/pdfmake.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/vendors/jquery-spinner/js/jquery.spinner.min.js')}}"></script>
    <script src="{{asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/slim/slim.kickstart.min.js')}}" type="text/javascript"></script>
@endsection
