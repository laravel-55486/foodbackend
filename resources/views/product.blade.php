@extends('layouts.app')
@section('title')
    Products
@endsection
@section('page_style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/dataTables.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/buttons.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/rowReorder.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/sweetalert/css/sweetalert.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/vendors/Buttons/css/buttons.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/pages/advbuttons.css')}}" />
    <link href="{{asset('assets/vendors/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/vendors/select2/css/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/plugins/slim/slim.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/tables.css')}}" />
@endsection
@section('custom_style')
    <style media="screen">
        .select2-search.select2-search--dropdown {
            display: none;
        }
        #product_table td {
            vertical-align: middle;
        }
    </style>
@endsection
@section('welcome_text')
    <h1>Products</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard')}}">
                <i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard
            </a>
        </li>
        <li class="active">Products</li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <button type="button" data-toggle="modal" data-target="#new_product_modal" class="pull-right btn btn-responsive button-alignment btn-info">New Product</button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable" style="overflow:auto;">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="beer" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Product Manage
                    </h3>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table table-striped table-bordered" id="product_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td align="center" >{{$product->id}}</td>
                                    <td>
                                        <div class="">
                                            <a href="{{url('product/view/'.$product->id)}}">
                                                <img style="width:100%;max-width: 290px;" src="{{asset('uploads/products/'.$product->cover_image)}}" alt="">
                                            </a>
                                        </div>
                                    </td>
                                    <td>{{$product->title}}</td>
                                    <td>{{$product->description}}</td>
                                    <td align="center">{{$product->price}} €</td>
                                    <td>
                                        <div class="">
                                            <a type="button" data-product_id="{{$product->id}}" style="margin-right:5px;padding:0 20px" class="product_edit_btn button-small button button-rounded button-info" >Edit</a>
                                            <a type="button" data-product_id="{{$product->id}}" style="margin-right:5px;padding:0 20px" class="product_delete_btn button-small button button-rounded button-danger">Delete</a>
                                            <a href="{{url('product/view/'.$product->id)}}" style="padding:0 20px" class="button-small button button-rounded button-success">Detail</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="new_product_modal" role="dialog" aria-labelledby="modalLabelfade" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade">Add New Product</h4>
                </div>
                <form id="new_product_add_form" action="{{route('product.add')}}" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div style="margin: 0 -15px;">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label" for="product_header_img">Product Image</label>
                                    <div id="product_header_img">
                                        <input type="file" name="slim[]" required/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="product_title">Product Title</label>
                                        <input type="text" class="form-control" name="product_title" placeholder="Product Title" id="product_title" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="spinners">
                                            <label class="control-label" for="product_price">Product Price</label>
                                            <div class="input-group">
                                                <input type="text" class="product_price_box form-control" name="product_price" placeholder="Product Price" id="product_price" required>
                                                <div class="input-group-addon">
                                                    €
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="product_description" >Product Description</label>
                                        <div class="input-group">
                                            <textarea name="product_description" id="product_description" placeholder="Write something here..." class="form-control resize_vertical " data-autogrow="" rows="5" cols="80" required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="edit_product_modal" role="dialog" aria-labelledby="modalLabelfade" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade">Update Product</h4>
                </div>
                <form id="edit_product_form" action="{{route('product.update')}}" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="product_id_edit" id="product_id_edit" value="">
                    <div class="modal-body">
                        <div style="margin: 0 -15px;">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label" for="edit_product_header_img">Product Image</label>
                                    <div id="edit_product_header_img">
                                        <img src="" alt="">
                                        <input type="file" name="slim[]"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="_product_title">Product Title</label>
                                        <input type="text" class="form-control" data-rule="currency" name="_product_title" placeholder="Product Title" id="_product_title" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="spinners">
                                            <label class="control-label" for="product_price">Product Price</label>
                                            <div class="input-group">
                                                <input type="text" class="product_price_box form-control" name="_product_price" placeholder="Product Price" id="_product_price" required>
                                                <div class="input-group-addon">
                                                    €
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="_product_description" >Product Description</label>
                                        <div class="input-group">
                                            <textarea name="_product_description" id="_product_description" placeholder="Write something here..." class="form-control resize_vertical " data-autogrow="" rows="5" cols="80" required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('custom_script')
    <script type="text/javascript" src="{{asset('js/admin_product.js')}}"></script>
@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jeditable/js/jquery.jeditable.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.rowReorder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/sweetalert/js/sweetalert.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/pdfmake.js')}}"></script>
    <script src="{{asset('assets/vendors/jquery-spinner/js/jquery.spinner.min.js')}}"></script>
    <script src="{{asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/slim/slim.kickstart.min.js')}}" type="text/javascript"></script>
@endsection
