@extends('layouts.app')
@section('title')
    Categories
@endsection
@section('page_style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/dataTables.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/buttons.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/rowReorder.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/sweetalert/css/sweetalert.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/vendors/Buttons/css/buttons.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/pages/advbuttons.css')}}" />
    <link href="{{asset('assets/vendors/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/vendors/select2/css/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/plugins/slim/slim.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/tables.css')}}" />
@endsection
@section('custom_style')
    <link href="{{asset('assets/css/pages/sortable_list.css')}}" rel="stylesheet" type="text/css">
    <style media="screen">
        .select2-search.select2-search--dropdown {
            display: none;
        }
        #category_table td {
            vertical-align: middle;
        }
    </style>
@endsection
@section('welcome_text')
    <h1>Categories</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard')}}">
                <i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard
            </a>
        </li>
        <li class="active">Categories</li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <button type="button" data-toggle="modal" data-target="#new_category_modal" class="pull-right btn btn-responsive button-alignment btn-info">New Category</button>
            <button style="margin-right: 15px;" type="button" data-toggle="modal" data-target="#category_sort_modal" class="pull-right btn btn-responsive button-alignment btn-success">Sort Category</button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable" style="overflow:auto;">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="tags" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Category Manage
                    </h3>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table table-striped table-bordered" id="category_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>position</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $category)
                                <tr>
                                    <td align="center" >{{$category->id}}</td>
                                    <td style="padding: 0;">
                                        <div class="">
                                            <a href="{{url('category/view/'.$category->id)}}" >
                                                <img style="width:100%;" src="{{asset('uploads/categories/'.$category->image)}}" alt="">
                                            </a>
                                        </div>
                                    </td>
                                    <td>{{$category->categoryName}}</td>
                                    <td>{{$category->categoryDescription}}</td>
                                    <td align="center">{{$category->position}}</td>
                                    <td>
                                        <div class="">
                                            <a data-category_id="{{$category->id}}" style="margin-right:5px;padding:0 20px" class="category_edit_btn button-small button button-rounded button-info" >Edit</button>
                                            <a data-category_id="{{$category->id}}" style="margin-right:5px;padding:0 20px" class="category_delete_btn button-small button button-rounded button-danger">Delete</button>
                                            <a href="{{url('category/view/'.$category->id)}}" style="padding:0 20px" class="button-small button button-rounded button-success">Detail</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="new_category_modal" role="dialog" aria-labelledby="modalLabelfade" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade">Add New Category</h4>
                </div>
                <form id="new_category_add_form" action="{{route('category.add')}}" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <label class="control-label" for="category_header_img">Category Image</label>
                        <div id="category_header_img">
                            <input type="file" name="slim[]" required/>
                        </div>
                        <div class="form-group">

                        </div>
                        <div class="form-group">
                            <label class="control-label" for="category_name">Category Name</label>
                            <input type="text" class="form-control" name="category_name" placeholder="Category name" id="category_name" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="category_position">Category Position</label>
                            <select id="category_position" name="category_position" class="form-control select2" required>
                                <option value="">0</option>
                                @for ($i=1; $i <= $position_num; $i++)
                                    <option value="{{$i}}" @if ($i == $position_num) selected @endif >{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="category_description" >Category Description</label>
                            <div class="input-group">
                                <textarea name="category_description" id="category_description" placeholder="Write something here..." class="form-control resize_vertical " data-autogrow="" rows="5" cols="80" required></textarea>
                            </div>
                            <!-- /.input group -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="edit_category_modal" role="dialog" aria-labelledby="modalLabelfade1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade1">Update Category</h4>
                </div>
                <form id="edit_category_add_form" action="{{route('category.update')}}" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="category_id_edit" id="category_id_edit" value="">
                    <div class="modal-body">
                        <label class="control-label" for="edit_category_header_img">Category Image</label>
                        <div id="edit_category_header_img">
                            <img src="" alt="">
                            <input type="file" name="slim[]"/>
                        </div>
                        <div class="form-group">

                        </div>
                        <div class="form-group">
                            <label class="control-label" for="_category_name">Category Name</label>
                            <input type="text" class="form-control" name="_category_name" placeholder="Category name" id="_category_name" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="_category_position">Category Position</label>
                            <select id="_category_position" name="_category_position" class="form-control select2" required>
                                <option value="">0</option>
                                @for ($i=1; $i < $position_num; $i++)
                                    <option value="{{$i}}" >{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="_category_description" >Category Description</label>
                            <div class="input-group">
                                <textarea name="_category_description" id="_category_description" placeholder="Write something here..." class="form-control resize_vertical " data-autogrow="" rows="5" cols="80" required></textarea>
                            </div>
                            <!-- /.input group -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="category_sort_modal" role="dialog" aria-labelledby="modalLabelfade2" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade2">Change Order</h4>
                </div>
                <form id="category_sort_form" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div data-force="30" class="layer block">
                            <ul id="foo" class="block__list block__list_words">
                                @foreach ($categories as $category)
                                    <li data-id="{{$category->id}}">{{$category->categoryName}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Save Order
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('custom_script')
    <script type="text/javascript">
        var local_category_sortable;
        var order_init = {{json_encode($orderlist)}};
        localStorage.setItem(local_category_sortable, order_init.join('|'));
    </script>
    <script src="{{asset('js/admin_category_sort.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('js/admin_category.js')}}"></script>
@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jeditable/js/jquery.jeditable.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.rowReorder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/sweetalert/js/sweetalert.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/pdfmake.js')}}"></script>
    <script src="{{asset('assets/vendors/Sortable/js/Sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/slim/slim.kickstart.min.js')}}" type="text/javascript"></script>
@endsection
