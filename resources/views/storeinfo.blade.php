@extends('layouts.app')
@section('title')
    Store Info
@endsection
@section('page_style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/gmaps/css/examples.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/googlemaps_custom.css')}}" />
    <link href="{{asset('assets/vendors/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/vendors/select2/css/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" media="screen" />
    <link href="{{asset('assets/plugins/slim/slim.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('custom_style')
    <link rel="stylesheet" href="/css/custom_admin.css">
@endsection
@section('welcome_text')
    <h1>Store Infomation</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard')}}">
                <i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard
            </a>
        </li>
        <li class="active">Store info</li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
               <div class="panel-heading">
                   <h3 class="panel-title" style="display: flex;align-items: center;">
                       <i class="livicon" data-name="map" data-size="25" data-loop="true" data-c="#fff" data-hc="white"></i>
                       &nbsp;
                       <span style="margin-top: 0;" >Store Location Map</span>
                   </h3>
               </div>
               <div class="panel-body" style="position: relative;">
                   <div class="map_background" id="gmap_hidden">
                       <h3>Went something wrong</h3>
                   </div>
                   <div style="width: 100%;height: 100%">
                       <div class="">
                           <h5 style="margin-top: 0;" >{{$storeinfo->street}}, {{$storeinfo->city}} {{$storeinfo->postalZip}}</h5>
                       </div>
                       <input id="pac-input" class="controls" type="text" placeholder="Search In Google Map">
                       <div id="map" class="map"></div>
                   </div>
               </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title" style="display: flex;align-items: center;">
                        <i class="livicon" data-name="location" data-size="25" data-loop="true" data-c="#fff" data-hc="white"></i>
                        &nbsp;
                        <span style="margin-top: 0;" >Store Infomation</span>
                    </h3>
                </div>
                <div class="panel-body">
                    <form action="{{route('storeinfo.ud.address')}}" id="storeinfo_address_save_form" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="coor_lat" id="coor_lat" value="{{$storeinfo->latitude}}">
                        <input type="hidden" name="coor_lng" id="coor_lng" value="{{$storeinfo->longitude}}">
                        <div style="margin: 0 -15px">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="store_street">Street</label>
                                        <input type="text" class="form-control" name="store_street" id="store_street" value="{{$storeinfo->street}}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="store_city">City</label>
                                        <input type="text" class="form-control" name="store_city" id="store_city" value="{{$storeinfo->city}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="store_zip">PostalZip</label>
                                        <input type="text" class="form-control" name="store_zip" id="store_zip" value="{{$storeinfo->postalZip}}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="store_country">Country</label>
                                        <select id="store_country" name="store_country" class="form-control select2">
                                            @foreach ($countries as $country)
                                                <option value="{{$country->code}}" @if ($country->code == $storeinfo->country_code) selected @endif>{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button class="col-xs-12 btn btn-info btn-load btn-md" type="submit" name="button">Save Address</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-body">
                    <form action="{{route('storeinfo.ud.basicinfo')}}" method="post">
                        {{ csrf_field() }}
                        <div style="margin: 0 -15px">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="inputSuccess">Store Name</label>
                                        <input type="text" class="form-control" name="store_name" id="store_name" value="{{$storeinfo->storeName}}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="inputSuccess">Business Name</label>
                                        <input type="text" class="form-control" name="store_bname" id="store_bname" value="{{$storeinfo->businessName}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="inputSuccess">Phone</label>
                                        <input type="text" class="form-control" name="store_phone" id="store_phone" value="{{$storeinfo->phone}}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label" for="inputSuccess">Delivery Cost</label>
                                    <div class="form-group input-group">
                                        <input type="text" class="form-control delivery_cost_box" name="store_delivery" id="store_delivery" value="{{$storeinfo->deliveryCost}}">
                                        <span class="input-group-addon">€</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button class="col-xs-12 btn btn-info btn-load btn-md" type="submit">Save Info</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title" style="display: flex;align-items: center;">
                        <i class="livicon" data-name="image" data-size="25" data-loop="true" data-c="#fff" data-hc="white"></i>
                        &nbsp;
                        <span style="margin-top: 0;" >App background Image</span>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="store-bg-img">
                        <img src="{{asset('uploads/storeinfo/'.$storeinfo->bg_img)}}" style="width: 100%;height: 100%;object-fit: cover;" alt="">
                        <button class="btn btn-responsive button-alignment btn-info" id="store-info-bg-change-btn" type="button" data-toggle="modal" data-target="#store_bg_modal">Change</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-info">
               <div class="panel-heading">
                   <h3 class="panel-title" style="display: flex;align-items: center;">
                       <i class="livicon" data-name="image" data-size="25" data-loop="true" data-c="#fff" data-hc="white"></i>
                       &nbsp;
                       <span style="margin-top: 0;" >App Header Image</span>
                   </h3>
               </div>
               <div class="panel-body">
                   <div class="store-header-img">
                       <img src="{{asset('uploads/storeinfo/'.$storeinfo->header_img)}}" style="width: 100%;height: 100%;object-fit: cover;" alt="">
                       <button class="btn btn-responsive button-alignment btn-info" id="store-info-header-change-btn" type="button" data-toggle="modal" data-target="#store_header_modal">Change</button>
                   </div>
               </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-info">
               <div class="panel-heading">
                   <h3 class="panel-title" style="display: flex;align-items: center;">
                       <i class="livicon" data-name="image" data-size="25" data-loop="true" data-c="#fff" data-hc="white"></i>
                       &nbsp;
                       <span style="margin-top: 0;" >App Logo Image</span>
                   </h3>
               </div>
               <div class="panel-body" style="display: flex;justify-content: center;">
                   <div class="store-header-img" style="display: table;text-align: center;">
                       <div style="display: table-cell;vertical-align: middle;">
                           <img src="{{asset('uploads/storeinfo/'.$storeinfo->logo_img)}}" style="width: 200px;" alt="">
                           <br />
                           <button style="margin-top: 15px;" class="btn btn-responsive button-alignment btn-info" type="button" data-toggle="modal" data-target="#store_app_logo_modal">Change</button>
                       </div>
                   </div>
               </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title" style="display: flex;align-items: center;">
                        <i class="livicon" data-name="stopwatch" data-size="25" data-loop="true" data-c="#fff" data-hc="white"></i>
                        &nbsp;
                        <span style="margin-top: 0;" >Store Opening Time</span>
                    </h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" action="{{route('storeinfo.ud.weektime')}}" method="post">
                        {{ csrf_field() }}
                        <fieldset>
                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="store_monday">Monday</label>
                                <div class="col-md-9">
                                    <input id="store_monday" name="store_monday" type="text" placeholder="Monday Time" value="{{$storeinfo->monday}}" class="form-control">
                                </div>
                            </div>
                            <!-- Email input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="store_tuesday">Tuesday</label>
                                <div class="col-md-9">
                                    <input id="store_tuesday" name="store_tuesday" type="text" placeholder="Tuesday Time" value="{{$storeinfo->tuesday}}" class="form-control">
                                </div>
                            </div>
                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="store_wednesday">Wednesday</label>
                                <div class="col-md-9">
                                    <input id="store_wednesday" name="store_wednesday" type="text" placeholder="Wednesday Time" value="{{$storeinfo->wednesday}}" class="form-control">
                                </div>
                            </div>
                            <!-- Email input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="store_thursday">Thursday</label>
                                <div class="col-md-9">
                                    <input id="store_thursday" name="store_thursday" type="text" placeholder="Thursday Time" value="{{$storeinfo->thursday}}" class="form-control">
                                </div>
                            </div>
                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="store_friday">Friday</label>
                                <div class="col-md-9">
                                    <input id="store_friday" name="store_friday" type="text" placeholder="Friday Time" value="{{$storeinfo->friday}}" class="form-control">
                                </div>
                            </div>
                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="store_saturday">Saturday</label>
                                <div class="col-md-9">
                                    <input id="store_saturday" name="store_saturday" type="text" placeholder="Saturday Time" value="{{$storeinfo->saturday}}" class="form-control">
                                </div>
                            </div>
                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="store_sunday">Sunday</label>
                                <div class="col-md-9">
                                    <input id="store_sunday" name="store_sunday" type="text" placeholder="Sunday Time" value="{{$storeinfo->sunday}}" class="form-control">
                                </div>
                            </div>
                            <!-- Form actions -->
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3 text-right">
                                    <button type="submit" class="col-xs-12 btn btn-responsive btn-info">Save</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title" style="display: flex;align-items: center;">
                        <i class="livicon" data-name="edit" data-size="25" data-loop="true" data-c="#fff" data-hc="white"></i>
                        &nbsp;
                        <span style="margin-top: 0;" >Store Description</span>
                    </h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" action="{{route('storeinfo.ud.description')}}" method="post">
                        {{ csrf_field() }}
                        <fieldset>
                            <!-- Name input-->
                            <textarea id="store_message" name="store_message">{{$storeinfo->message}}</textarea>
                            <!-- Form actions -->
                            <div class="form-group" style="margin-top: 10px;">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="col-xs-12 btn btn-responsive btn-info">Save</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="store_header_modal" role="dialog" aria-labelledby="modalLabelfade" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade">Choose Header Image</h4>
                </div>
                <form action="{{route('storeinfo.ud.headerimg')}}" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div id="store-app-header-image">
                            <input type="file" name="slim[]" required/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="store_bg_modal" role="dialog" aria-labelledby="modalLabelfade1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade1">Choose Background Image</h4>
                </div>
                <form action="{{route('storeinfo.ud.bgimg')}}" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div id="store-app-bg-image">
                            <input type="file" name="slim[]" required/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="store_app_logo_modal" role="dialog" aria-labelledby="modalLabelfade2" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade2">Choose App Logo</h4>
                </div>
                <form action="{{route('storeinfo.ud.logoimg')}}" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div id="store-app-logo-image">
                            <input type="file" name="slim[]" required/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('custom_script')
    <script type="text/javascript">
        var coor_lng = {{$storeinfo->longitude}};
        var coor_lat = {{$storeinfo->latitude}};
        var map, marker, coordinatelat, coordinatelng;

        if (coor_lng != null && coor_lng != "" && coor_lat != null && coor_lat != "") {
            coordinatelat = Number(coor_lat);
            coordinatelng = Number(coor_lng);
            mapInit();
        } else {
            $('#gmap_hidden').fadeIn();
        }

        function mapInit() {

            var mapOptions = {
        		center: new google.maps.LatLng(coordinatelat,coordinatelng),
        		zoom: 15,
        		mapTypeId: google.maps.MapTypeId.ROADMAP,
                gestureHandling: 'cooperative'
        	};
            map = new google.maps.Map(document.getElementById('map'),mapOptions);

            var myLatLng = {lat: coordinatelat, lng: coordinatelng};

            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
              searchBox.setBounds(map.getBounds());
            });

            marker = new google.maps.Marker({
              map: map,
              position: myLatLng
            });

            var markers = [];
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }

                    marker.setMap(null);
                    marker = '';

                    // Create a marker for each place.
                    marker = new google.maps.Marker({
                        map: map,
                        title: place.name,
                        position: place.geometry.location
                    });

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }

        function createMarker(latlng) {

          if(marker != undefined && marker != ''){
            marker.setMap(null);
            marker = '';
          }

          marker = new google.maps.Marker({
            map: map,
            position: latlng
          });
        }

        var store_header_cropper = new Slim(document.getElementById('store-app-header-image'), {
            ratio: '5:3',
            minSize: {
                width: 150,
                height: 90,
            },
            download: false,
            label: 'Drop your image here or Click.',
            statusImageTooSmall:'Image too small. Min Size is $0 pixel. Try again',
        });
        store_header_cropper.size = { width:2000, height:1200 };

        var store_bg_cropper = new Slim(document.getElementById('store-app-bg-image'), {
            ratio: '3:2',
            minSize: {
                width: 150,
                height: 90,
            },
            download: false,
            label: 'Drop your image here or Click.',
            statusImageTooSmall:'Image too small. Min Size is $0 pixel. Try again',
        });
        store_bg_cropper.size = { width:2000, height:1200 };

        var store_app_logo_cropper = new Slim(document.getElementById('store-app-logo-image'), {
            minSize: {
                width: 50,
                height: 50,
            },
            download: false,
            label: 'Drop your image here or Click.',
            statusImageTooSmall:'Image too small. Min Size is $0 pixel. Try again',
        });
        store_app_logo_cropper.size = { width:1000, height:1000 };

        $("#store_country").select2({
            theme: "bootstrap",
            placeholder: "select a Country"
        });

        $('#store_street, #store_city, #store_country').on('change',function(e) {
            resetmap();
        });

        function resetmap () {
            var street = document.getElementById('store_street').value;
            var city = document.getElementById('store_city').value;
            var countryId = document.getElementById('store_country');
            var countryName = countryId.options[countryId.selectedIndex].text;

            var addressInput = "";

            if (street != "")
            {
                addressInput = addressInput + street + ",";
            }

            if (city != "")
            {
                addressInput = addressInput + city+",";
            }

            if (countryName != "")
            {
                addressInput = addressInput + countryName;
            }

        	var geocoder = new google.maps.Geocoder();

        	geocoder.geocode({address: addressInput}, function(results, status) {

        		if (status == google.maps.GeocoderStatus.OK) {

                var myResult = results[0].geometry.location;

                $('#coor_lat').val(myResult.lat());
                $('#coor_lng').val(myResult.lng());
                createMarker(myResult);

                map.setCenter(myResult);
                }
            });
        }

        tinymce.init({
            selector: "#store_message",
            height: '255px',
            menubar:false,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify"
        });

        $('#storeinfo_address_save_form').on('submit', function(e) {
            e.preventDefault();
            var form = $(this)[0];
            resetmap();
            setInterval(function() {
                var new_coor_lng = $('#coor_lng').val();
                if (new_coor_lng != coor_lng) {
                    form.submit();
                }
            }, 1000)
        });

        $('.delivery_cost_box').on('keypress', function(e) {
            var $this = $(this);
            var available_keys = [44,45,46,48,49,50,51,52,53,54,55,56,57];
            var current_code = parseInt(e.which);
            if ($.inArray(current_code, available_keys) > -1) {

            } else {
                return false;
            }
        });

        $('.delivery_cost_box').on('keyup', function(e) {
            var $this = $(this);
            if (e.keyCode == 188) {
                var current_string = $this.val();
                current_string = current_string.replace(/,/g , ".");
                $this.val(current_string);
            }
        });
    </script>
@endsection
@section('page_script')
    <!-- begining of page level js -->
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyBJmvYsJRi6GEsOyQwTtUPXA0gmQKZ6cNc&libraries=places"></script>
    <script src="{{asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/gmaps/js/gmaps.min.js')}}"></script>
    <script src="{{asset('assets/vendors/tinymce/tinymce.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/slim/slim.kickstart.min.js')}}" type="text/javascript"></script>
    <!-- end of page level js -->
@endsection
