@extends('layouts.app')
@section('title')
    Extras
@endsection
@section('page_style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/dataTables.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/buttons.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/rowReorder.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/sweetalert/css/sweetalert.css')}}" />
    <link href="{{asset('assets/vendors/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/vendors/select2/css/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/plugins/slim/slim.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/tables.css')}}" />
@endsection
@section('custom_style')
    <style media="screen">
        .select2-search.select2-search--dropdown {
            display: none;
        }
        #extra_table td {
            vertical-align: middle;
        }
    </style>
@endsection
@section('welcome_text')
    <h1>Extras</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard')}}">
                <i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard
            </a>
        </li>
        <li class="active">Extras</li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <button type="button" data-toggle="modal" data-target="#new_extra_modal" class="pull-right btn btn-responsive button-alignment btn-info">New Extra</button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable" style="overflow:auto;">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="clip" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Extra Manage
                    </h3>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table table-striped table-bordered" id="extra_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($extras as $extra)
                                <tr>
                                    <td align="center" >{{$extra->id}}</td>
                                    <td>{{$extra->name}}</td>
                                    <td>{{$extra->price}} €</td>
                                    <td>
                                        <div class="">
                                            <button type="button" data-extra_id="{{$extra->id}}" class="extra_edit_btn btn btn-responsive button-alignment btn-info" >Edit</button>
                                            <button type="button" data-extra_id="{{$extra->id}}" class="extra_delete_btn btn btn-responsive button-alignment btn-danger">Delete</button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="new_extra_modal" role="dialog" aria-labelledby="modalLabelfade" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade">Add New Extra</h4>
                </div>
                <form id="new_extra_add_form" action="{{route('extra.add')}}" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label" for="extra_name">Extra Title</label>
                            <input type="text" class="form-control" name="extra_name" placeholder="Extra Title" id="extra_name" required>
                        </div>
                        <div class="form-group">
                            <div class="spinners">
                                <label class="control-label" for="extra_price">Extra Price</label>
                                <div class="input-group">
                                    <input type="text" class="form-control extra_price_box" name="extra_price" placeholder="Extra Price" id="extra_price" required>
                                    <div class="input-group-addon">
                                        €
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="edit_extra_modal" role="dialog" aria-labelledby="modalLabelfade" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="modalLabelfade">Update Extra</h4>
                </div>
                <form id="edit_extra_form" action="{{route('extra.update')}}" role="form" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="extra_id_edit" id="extra_id_edit" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label" for="_extra_name">Extra Title</label>
                            <input type="text" class="form-control" name="_extra_name" placeholder="Extra Title" id="_extra_name" required>
                        </div>
                        <div class="form-group">
                            <div class="spinners">
                                <label class="control-label" for="extra_price">Extra Price</label>
                                <div class="input-group">
                                    <input type="text" class="form-control extra_price_box" name="_extra_price" placeholder="Extra Price" id="_extra_price" required>
                                    <div class="input-group-addon">
                                        €
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-responsive button-alignment btn-info">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('custom_script')
    <script type="text/javascript" src="{{asset('js/admin_extra.js')}}"></script>
@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jeditable/js/jquery.jeditable.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.rowReorder.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/sweetalert/js/sweetalert.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/pdfmake.js')}}"></script>
    <script src="{{asset('assets/vendors/jquery-spinner/js/jquery.spinner.min.js')}}"></script>
    <script src="{{asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/slim/slim.kickstart.min.js')}}" type="text/javascript"></script>
@endsection
