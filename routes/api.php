<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\ApiController@login');
Route::post('register', 'Api\ApiController@register');
Route::post('sendResetEmail', 'Api\ApiController@SendResetCode');
Route::post('passwordReset', 'Api\ApiController@passwordreset');

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('details', 'Api\ApiController@details');
    Route::get('couponCheck/{couponString}', 'Api\ApiController@cuponCheck');
    Route::get('clearBadge', 'Api\ApiController@clearBadge');
    Route::get('getpushhistory', 'Api\ApiController@getPushHistory');
    Route::get('setPushHistory', 'Api\ApiController@setPushHistoryBadge');
    Route::get('getbadgeNumber', 'Api\ApiController@getBadgeNumber');
});

Route::get('sendautherror', function(){
    return response()->json(['result' => 'error']);
});

//Logo
Route::get('logoImg', 'ApiController@logoImg');

//Header
Route::get('headerImg', 'ApiController@headerImg');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// List catalog
Route::get('catalog/{id}', 'Api\ApiController@getCatalogs');

//Get specific product information
Route::get('singleExtras/{product_id}', 'Api\ApiController@getExtrasProduct');

//Get store information
Route::get('storeinfo/{store_id}/{storeinfo}', 'Api\ApiController@storeInfo');

// Send out a new order to mysql
Route::post('newOrder', 'Api\ApiController@store');

//Send an order to the database
Route::post('makeOrder', 'Api\ApiController@store');
