<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/dashboard', 'AdminController@index')->name('dashboard');
Route::prefix('profile')->group(function () {
    Route::get('/', 'AdminController@profile')->name('profile');
    Route::post('/update/avatar', 'AdminController@updateAvatar')->name('profile.ud.avatar');
    Route::post('/update/f_name', 'AdminController@updateBio')->name('profile.ud.bio');
    Route::post('/update/password', 'AdminController@updatePass')->name('profile.ud.pass');
});

Route::prefix('storeinfo')->group(function () {
    Route::get('/', 'StoreInfoController@index')->name('storeinfo');
    Route::post('/update/address', 'StoreInfoController@updateAddress')->name('storeinfo.ud.address');
    Route::post('/update/headerimg', 'StoreInfoController@updateHeaderimg')->name('storeinfo.ud.headerimg');
    Route::post('/update/bgimg', 'StoreInfoController@updateBgimg')->name('storeinfo.ud.bgimg');
    Route::post('/update/logoimg', 'StoreInfoController@updateLogoimg')->name('storeinfo.ud.logoimg');
    Route::post('/update/basicinfo', 'StoreInfoController@updateBasicinfo')->name('storeinfo.ud.basicinfo');
    Route::post('/update/description', 'StoreInfoController@updateDescription')->name('storeinfo.ud.description');
    Route::post('/update/weektime', 'StoreInfoController@updateWeektime')->name('storeinfo.ud.weektime');
});

Route::prefix('category')->group(function () {
    Route::get('/', 'FoodController@categoryShow')->name('category');
    Route::post('/add', 'FoodController@categoryAdd')->name('category.add');
    Route::post('/update', 'FoodController@categoryUpdate')->name('category.update');
    Route::get('/getsingle/{id}', 'FoodController@getSingleCategory');
    Route::get('/delete/{id}', 'FoodController@categoryDestroy');
    Route::get('/view/{id}', 'FoodAssignController@viewsingleCategory')->name('category.product');
    Route::post('/add/product', 'FoodAssignController@categoryAddProducts')->name('category.add.products');
    Route::get('/product/change/position/{catproid}/{position}', 'FoodAssignController@catProductChangePosition');
    Route::get('/product/delete/{id}', 'FoodAssignController@categoryProductDestroy');
    Route::post('/setOrder', 'FoodController@categorySetOrder');
    Route::post('/product/setOrder', 'FoodAssignController@categoryProductSetOrder');
});

Route::prefix('product')->group(function () {
    Route::get('/', 'FoodController@productShow')->name('product');
    Route::post('/add', 'FoodController@productAdd')->name('product.add');
    Route::post('/update', 'FoodController@productUpdate')->name('product.update');
    Route::get('/getsingle/{id}', 'FoodController@getSingleProduct');
    Route::get('/delete/{id}', 'FoodController@productDestroy');
    Route::get('/view/{id}', 'FoodAssignController@viewsingleProduct')->name('product.extra');
    Route::post('/add/extra', 'FoodAssignController@productAddExtras')->name('product.add.extra');
    Route::get('/extra/change/position/{proId}/{position}', 'FoodAssignController@proExtraChangePosition');
    Route::get('/extra/delete/{id}', 'FoodAssignController@productExtraDestroy');
    Route::post('/extra/setOrder', 'FoodAssignController@productExtraSetOrder');
});

Route::prefix('extra')->group(function () {
    Route::get('/', 'FoodController@extraShow')->name('extra');
    Route::post('/add', 'FoodController@extraAdd')->name('extra.add');
    Route::post('/update', 'FoodController@extraUpdate')->name('extra.update');
    Route::get('/getsingle/{id}', 'FoodController@getSingleExtra');
    Route::get('/delete/{id}', 'FoodController@extraDestroy');
});

Route::prefix('cupon')->group(function () {
    Route::get('/', 'AdminController@viewCupon')->name('cupon');
    Route::post('/add', 'AdminController@cuponAdd')->name('cupon.add');
    Route::post('/update', 'AdminController@cuponUpdate')->name('cupon.update');
    Route::get('/getsingle/{id}', 'AdminController@getSingleCupon');
    Route::get('/delete/{id}', 'AdminController@cuponDestroy');
});

Route::prefix('user')->group(function () {
    Route::get('/', 'NotificationController@viewUser')->name('user');
    Route::get('/detail/{id}', 'NotificationController@userDetail')->name('user.detail');
    Route::get('/delete/{id}', 'NotificationController@userDestroy');
});

Route::prefix('push')->group(function () {
    Route::get('/', 'NotificationController@viewPush')->name('push');
    Route::post('/notify', 'NotificationController@sendNotify')->name('push.notify.send');
});
