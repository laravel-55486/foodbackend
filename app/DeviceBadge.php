<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Mpociot\Firebase\SyncsWithFirebase;

class DeviceBadge extends Model
{
    // use SyncsWithFirebase;

    protected $table = 'device_badges';

    protected $fillable = [
        'user_id', 'badge_num',
    ];

    public $timestamps = false;
}
