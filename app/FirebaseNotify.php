<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FirebaseNotify extends Model
{

    protected $table = 'user_notifications';

    protected $fillable = [
        'user_id', 'device_fcm', 'msg_title', 'msg_text', 'badge',
    ];
}
