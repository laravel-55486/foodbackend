<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PushLog extends Model
{
    protected $table = 'push_logs';

    protected $fillable = [
        'user_id', 'msg_title', 'msg_text',
    ];
}
