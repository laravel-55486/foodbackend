<?php

namespace App\Http\Resources;

use App\Http\Resources\ExtraResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            "id" => $this->id,
            "title" => $this->title,
            "description" => $this->description,
            "price" => $this->price,
            "image" => asset('uploads/products/'.$this->cover_image),
            "position" => $this->pivot->position,
            "extras" => ExtraResource::collection($this->extras)->sortBy('position')
        ];
    }
}
