<?php

namespace App\Http\Controllers;

use File;
use Auth;
use DateTime;
use App\Slim;
use App\User;
use App\Admin;
use App\Cupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $app_view = array();
        $app_view['day'] = 2300;
        $app_view['week'] = 36400;
        $app_view['month'] = 182000;

        $app_order = array();
        $app_order['day'] = 57;
        $app_order['week'] = 455;
        $app_order['month'] = 2270;

        $app_cash = array();
        $app_cash['day'] = 3700;
        $app_cash['week'] = 25900;
        $app_cash['month'] = 129500;

        $app_register = array();
        $app_register['day'] = 2700;
        $app_register['week'] = 15900;
        $app_register['month'] = 79500;

        $app_view_object = (object) $app_view;
        $app_order_object = (object) $app_order;
        $app_cash_object = (object) $app_cash;
        $app_register_object = (object) $app_register;
        return view('dashboard', ['user_count' => $app_view_object, 'order_count' => $app_order_object, 'cash_count' => $app_cash_object, 'register_count' => $app_register_object]);
    }

    public function profile()
    {
        return view('profile');
    }

    public function updateAvatar(Request $request)
    {
        $admin = Auth::user();
        $imageRand = rand(1000, 9999);
        $random_name = $imageRand."_".time();

        if(!is_dir(public_path('avatars/admin'))){
            mkdir(public_path('avatars/admin'));
        }

        $dst = public_path('avatars/admin/');

        if($admin->avatar != 'default.jpg') {
            if (File::exists($dst . $admin->avatar)) {
                File::delete($dst . $admin->avatar);
            }
        }

        $finish_image = $this->uploadImagetoServer($request, $random_name, $dst);

        if ($finish_image['result'] == "success") {
            $admin->avatar = $finish_image['image'][0]['name'];
            $admin->save();

            return back()->with('success', 'Avatar changed successfully.');
        }

        return back()->with('error', 'went something wrong');
    }

    public function uploadImagetoServer($imgdata, $name, $path)
    {
        $files = array();
        $result = array();
        $rules = [
            'file' => 'image',
            'slim[]' => 'image'
        ];

        $validator = Validator::make($imgdata->all(), $rules);
        $errors = $validator->errors();

        if($validator->fails()){
            $result = array('result' => 'fail', 'reson' => 'validator');
            return $result;
        }

        // Get posted data
        $images = Slim::getImages();

        // No image found under the supplied input name
        if ($images == false) {
            $result = array('result' => 'fail', 'reson' => 'image');
            return $result;
        } else {
            foreach ($images as $image) {
                // save output data if set
                if (isset($image['output']['data'])) {
                    // Save the file
                    $origine_name = $image['input']['name'];
                    $file_type = pathinfo($origine_name, PATHINFO_EXTENSION);
                    $finalName = $name.".".$file_type;

                    // We'll use the output crop data
                    $data = $image['output']['data'];
                    $output = Slim::saveFile($data, $finalName, $path, false);
                    array_push($files, $output);
                    $result = array('result' => 'success', 'image' => $files);
                    return $result;
                }
                // save input data if set
                if (isset ($image['input']['data'])) {
                    // Save the file
                    $origine_name = $image['input']['name'];
                    $file_type = pathinfo($origine_name, PATHINFO_EXTENSION);
                    $finalName = $name.".".$file_type;

                    $data = $image['input']['data'];
                    $input = Slim::saveFile($data, $finalName, $path, false);
                    array_push($files, $output);

                    $result = array('result' => 'success', 'image' => $files);
                    return $result;
                }
            }
        }
    }

    public function updateBio(Request $request)
    {
        $admin = Auth::user();
        $sql_field = $request->name;
        if ($sql_field == "first_name") {
            $admin->first_name = $request->value;
        }

        if ($sql_field == "last_name") {
            $admin->last_name = $request->value;
        }

        if ($sql_field == "email") {
            $admin->email = $request->value;
        }

        $admin->save();
        return response()->json(['result' => 'success']);
    }

    public function updatePass(Request $request)
    {
        if (Auth::check()) {
            $admin = Auth::user();
            $admin->password = bcrypt($request->new_password);
            $admin->save();
            return back()->with('success', 'Password changed successfully.');
        }
        return redirect('login');
    }

    public function viewCupon()
    {
        $users = User::all();
        $cupons = Cupon::join('users', 'users.id', '=', 'cupons.user_id')->select('users.name as username','cupons.*')->get();
        return view('userCupon', ['cupons' => $cupons, 'users' => $users]);
    }

    public function cuponAdd(Request $request)
    {
        // dd($request->all());
        $user_ids = $request->selected_users;
        foreach ($user_ids as $cupon_user_id) {
            $percent = (float) $request->cupon_percent;
            $new_cupon = new Cupon;
            $new_cupon->discountName = $request->cupon_name;
            $new_cupon->percentage = $percent;
            $new_cupon->user_id = $cupon_user_id;
            $new_cupon->save();
        }

        return back()->with('success', 'added new Cupon');
    }

    public function getSingleCupon($id)
    {
        $cupon = Cupon::where('cupons.id', $id)->join('users', 'users.id', '=', 'cupons.user_id')->select('users.name as username', 'cupons.*')->first();
        if ($cupon) {
            return response()->json(['result' => 'success', 'cupon' => $cupon]);
        }

        return response()->json(['result' => 'error']);
    }

    public function cuponUpdate(Request $request)
    {
        $edit_cupon = Cupon::find($request->cupon_id_edit);
        if ($edit_cupon) {
            $percent = (float) $request->_cupon_percent;
            $edit_cupon->discountName = $request->_cupon_name;
            $edit_cupon->percentage = $percent;
            $edit_cupon->user_id = $request->_cupon_user;
            $edit_cupon->save();
            return back()->with('success', 'Updated Cupon');
        }
        return back()->with('error', 'can not find Cupon');
    }

    public function cuponDestroy($id)
    {
        $delete_cupon = Cupon::find($id);
        if ($delete_cupon) {
            $delete_cupon->delete();
            return back()->with('success', 'Cupon Deleted');
        }
        return back()->with('error', 'can not find Cupon');
    }
}
