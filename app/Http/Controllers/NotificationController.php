<?php

namespace App\Http\Controllers;

use FCM;
use App\User;
use App\PushLog;
use App\DeviceBadge;
use App\FirebaseNotify;
use Illuminate\Http\Request;
use LaravelFCM\Message\PayloadNotificationBuilder;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewUser()
    {
        $users = User::all();

        return view('user', ['users' => $users]);
    }

    public function viewPush()
    {
        $users = User::all();
        $notifications = PushLog::join('users','users.id', '=', 'push_logs.user_id')->select('push_logs.*', 'users.name')->get();

        return view('push', ['notifications' => $notifications, 'users' => $users]);
    }

    public function userDestroy($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->delete();
            return back()->with('success', 'User Deleted');
        }
        return back()->with('error', 'Can not find user');
    }

    public function sendNotify(Request $request)
    {
        $selected_users = $request->selected_users;
        foreach ($selected_users as $selected_user) {
            $user = User::find($selected_user);
            if ($user) {

                $budge_number = 1;
                $current_badge_count = DeviceBadge::where('user_id', $user->id)->count();
                if ($current_badge_count > 0) {
                    $current_badge = DeviceBadge::where('user_id', $user->id)->first();
                    $current_badge->badge_num = $current_badge->badge_num+1;
                    $current_badge->save();

                    $budge_number = $current_badge->badge_num;
                } else {
                    DeviceBadge::create([
                        'user_id' => $user->id,
                        'badge_num' => 1
                    ]);
                }

                $user_devices = unserialize($user->device_fcm);

                foreach ($user_devices as $user_device) {
                    if($user_device) {
                        FirebaseNotify::create([
                            'user_id' => $user->id,
                            'device_fcm' => $user_device,
                            'msg_title' => $request->notify_title,
                            'msg_text' => $request->notify_text,
                            'badge' => $budge_number,
                        ]);

                        $notificationBuilder = new PayloadNotificationBuilder($request->notify_title);
                        $notificationBuilder->setBody($request->notify_text)
                        				    ->setSound('default')
                                            ->setBadge($budge_number);
                        $notification = $notificationBuilder->build();

                        $downstreamResponse = FCM::sendTo($user_device, null, $notification, null);

                        $downstreamResponse->numberSuccess();
                        $downstreamResponse->numberFailure();
                        $downstreamResponse->numberModification();
                        $downstreamResponse->tokensToDelete();
                        $downstreamResponse->tokensToModify();
                        $downstreamResponse->tokensToRetry();
                    }
                }

                PushLog::create([
                    'user_id' => $user->id,
                    'msg_title' => $request->notify_title,
                    'msg_text' => $request->notify_text,
                ]);
            }
        }

        return back()->with('success', 'Sent Notification');
    }

    public function userDetail($id)
    {
        $user = User::find($id);
        if ($user) {
            $notifications = FirebaseNotify::where('user_id', $user->id)->get();

            return view('userDetail', ['user' => $user, 'notifications' => $notifications]);
        }

        return back()->with('error', 'Can not find user');
    }
}
