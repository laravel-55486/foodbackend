<?php

namespace App\Http\Controllers;

use File;
use Auth;
use DateTime;
use App\Slim;
use App\Extra;
use App\Product;
use App\Category;
use App\ProductExtra;
use App\CategoryProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FoodAssignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewsingleCategory($id)
    {
        $category = Category::find($id);
        if ($category) {
            $cat_products = CategoryProduct::where('category_id', $id)
                            ->join('products', 'products.id', '=', 'category_products.product_id')
                            ->select('products.cover_image','products.title','products.description','products.price', 'category_products.id','category_products.position')->get();

            $products = Product::all();
            $final_products = array();
            foreach ($products as $product) {
                $check_already = CategoryProduct::where('category_id', $id)->where('product_id', $product->id)->count();
                if ($check_already == 0) {
                    array_push($final_products,$product);
                }
            }

            $count_category_amount = CategoryProduct::where('category_id', $id)->count();
            $order_cat_products = CategoryProduct::where('category_id', $id)->orderBy('position', 'ASC')->get();
            $order_list = array();
            if ($order_cat_products) {
                foreach ($order_cat_products as $order_cat_product) {
                    array_push($order_list, $order_cat_product->id);
                }
            }
            return view('categoryProduct', ['cat_products' => $cat_products, 'products' => $final_products, 'category' => $category, 'position_nums' => $count_category_amount,'orderlist' => $order_list]);
        }

        return back()->with('error', 'went something wrong');
    }

    public function categoryProductSetOrder(Request $request)
    {
        $new_sort = $request->sort_list;
        $position = 1;
        for ($i=0; $i < count($new_sort); $i++) {
            $cat_product = CategoryProduct::find($new_sort[$i]);
            if ($cat_product) {
                $cat_product->position = $position;
                $cat_product->save();
                $position++;
            }
        }
        return $new_sort;
    }

    public function categoryAddProducts(Request $request)
    {
        $selected_products = $request->selected_products;

        foreach ($selected_products as $selected_product) {
            $check_already = CategoryProduct::where('category_id', $request->category_id_product_add)->where('product_id', $selected_product)->count();
            $count_position = CategoryProduct::where('category_id', $request->category_id_product_add)->count();
            if ($check_already == 0) {
                $now_position = $count_position + 1;
                $cat_product = new CategoryProduct;
                $cat_product->category_id = $request->category_id_product_add;
                $cat_product->product_id = $selected_product;
                $cat_product->position = $now_position;
                $cat_product->save();
            }
        }

        return back();
    }

    public function catProductChangePosition($catproId, $position)
    {
        $edit_cat_product = CategoryProduct::find($catproId);

        $cat_products = CategoryProduct::where('id', '!=', $catproId)->orderBy('position', 'ASC')->get();
        if ($cat_products) {
            foreach ($cat_products as $cat_product) {
                if ($cat_product->position == $position) {
                    $cat_product->position = $edit_cat_product->position;
                    $cat_product->save();
                }
            }
        }

        $edit_cat_product->position = $position;
        $edit_cat_product->save();

        return back()->with('success', 'updated position');
    }

    public function categoryProductDestroy($id)
    {
        $delete_cat_product = CategoryProduct::find($id);
        $delete_cat_product->delete();

        $cat_products = CategoryProduct::orderBy('position', 'ASC')->get();
        $position = 1;

        foreach ($cat_products as $cat_product) {
            $cat_product->position = $position;
            $cat_product->save();
            $position++;
        }

        return back()->with('success', 'product Removed');
    }

    public function viewsingleProduct($id)
    {
        $product = Product::find($id);
        if ($product) {
            $pro_extras = ProductExtra::where('product_id', $id)
                            ->join('extras', 'extras.id', '=', 'product_extras.extra_id')
                            ->select('extras.name','extras.price', 'product_extras.id','product_extras.position')->get();

            $extras = Extra::all();
            $final_extras = array();
            foreach ($extras as $extra) {
                $check_already = ProductExtra::where('product_id', $id)->where('extra_id', $extra->id)->count();
                if ($check_already == 0) {
                    array_push($final_extras,$extra);
                }
            }

            $count_product_amount = ProductExtra::where('product_id', $id)->count();

            $order_pro_extras = ProductExtra::where('product_id', $id)->orderBy('position', 'ASC')->get();
            $order_list = array();
            if ($order_pro_extras) {
                foreach ($order_pro_extras as $order_pro_extra) {
                    array_push($order_list, $order_pro_extra->id);
                }
            }
            return view('productExtra', ['pro_extras' => $pro_extras, 'extras' => $final_extras, 'product' => $product, 'position_nums' => $count_product_amount,'orderlist' => $order_list]);
        }

        return back()->with('error', 'went something wrong');
    }

    public function productExtraSetOrder(Request $request)
    {
        $new_sort = $request->sort_list;
        $position = 1;
        for ($i=0; $i < count($new_sort); $i++) {
            $pro_extra = ProductExtra::find($new_sort[$i]);
            if ($pro_extra) {
                $pro_extra->position = $position;
                $pro_extra->save();
                $position++;
            }
        }
        return $new_sort;
    }

    public function productAddExtras(Request $request)
    {
        $selected_extras = $request->selected_extras;

        foreach ($selected_extras as $selected_extra) {
            $check_already = ProductExtra::where('product_id', $request->product_id_extra_add)->where('extra_id', $selected_extra)->count();
            $count_position = ProductExtra::where('product_id', $request->product_id_extra_add)->count();
            if ($check_already == 0) {
                $now_position = $count_position + 1;
                $pro_extra = new ProductExtra;
                $pro_extra->product_id = $request->product_id_extra_add;
                $pro_extra->extra_id = $selected_extra;
                $pro_extra->position = $now_position;
                $pro_extra->save();
            }
        }

        return back();
    }

    public function proExtraChangePosition($proextraId, $position)
    {
        $edit_pro_extra = ProductExtra::find($proextraId);

        $pro_extras = ProductExtra::where('id', '!=', $proextraId)->orderBy('position', 'ASC')->get();
        if ($pro_extras) {
            foreach ($pro_extras as $pro_extra) {
                if ($pro_extra->position == $position) {
                    $pro_extra->position = $edit_pro_extra->position;
                    $pro_extra->save();
                }
            }
        }

        $edit_pro_extra->position = $position;
        $edit_pro_extra->save();

        return back()->with('success', 'updated position');
    }

    public function productExtraDestroy($id)
    {
        $delete_pro_extra = ProductExtra::find($id);
        $delete_pro_extra->delete();

        $pro_extras = ProductExtra::orderBy('position', 'ASC')->get();
        $position = 1;

        foreach ($pro_extras as $pro_extra) {
            $pro_extra->position = $position;
            $pro_extra->save();
            $position++;
        }

        return back()->with('success', 'extra Removed');
    }
}
