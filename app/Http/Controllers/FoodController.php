<?php

namespace App\Http\Controllers;

use File;
use Auth;
use DateTime;
use App\Slim;
use App\Extra;
use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class FoodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function categoryShow()
    {
        $categories = Category::orderBy('position', 'ASC')->get();
        $category_count = Category::count();
        $category_count = $category_count+1;
        $order_list = array();
        if ($categories) {
            foreach ($categories as $category) {
                array_push($order_list, $category->id);
            }
        }
        return view('category',['categories' => $categories,'position_num' => $category_count, 'orderlist' => $order_list]);
    }

    public function categorySetOrder(Request $request)
    {
        $new_sort = $request->sort_list;
        $position = 1;
        for ($i=0; $i < count($new_sort); $i++) {
            $category = Category::find($new_sort[$i]);
            if ($category) {
                $category->position = $position;
                $category->save();
                $position++;
            }
        }
        return $new_sort;
    }

    public function categoryAdd(Request $request)
    {
        $imagename = "category"."_".time();

        if(!is_dir(public_path('uploads/categories'))){
            mkdir(public_path('uploads/categories'));
        }

        $dst = public_path('uploads/categories/');


        $finish_image = $this->uploadImagetoServer($request, $imagename, $dst);

        if ($finish_image['result'] == "success") {
            $categories = Category::orderBy('position', 'ASC')->get();
            $total_num = Category::count();
            if ($categories) {
                $position_number = $request->category_position;
                foreach ($categories as $category) {
                    if ($category->position == $position_number) {
                        $category->position = $total_num+1;
                        $category->save();
                    }
                }
            }

            $new_category = new Category;
            $new_category->image = $finish_image['image'][0]['name'];
            $new_category->categoryName = $request->category_name;
            $new_category->categoryDescription = $request->category_description;
            $new_category->position = $request->category_position;
            $new_category->save();

            return back()->with('success', 'New Category created successfully.');
        }
    }

    public function uploadImagetoServer($imgdata, $name, $path)
    {
        $files = array();
        $result = array();
        $rules = [
            'file' => 'image',
            'slim[]' => 'image'
        ];

        $validator = Validator::make($imgdata->all(), $rules);
        $errors = $validator->errors();

        if($validator->fails()){
            $result = array('result' => 'fail', 'reson' => 'validator');
            return $result;
        }

        // Get posted data
        $images = Slim::getImages();

        // No image found under the supplied input name
        if ($images == false) {
            $result = array('result' => 'fail', 'reson' => 'image');
            return $result;
        } else {
            foreach ($images as $image) {
                // save output data if set
                if (isset($image['output']['data'])) {
                    // Save the file
                    $origine_name = $image['input']['name'];
                    $file_type = pathinfo($origine_name, PATHINFO_EXTENSION);
                    $finalName = $name.".".$file_type;

                    // We'll use the output crop data
                    $data = $image['output']['data'];
                    $output = Slim::saveFile($data, $finalName, $path, false);
                    array_push($files, $output);
                    $result = array('result' => 'success', 'image' => $files);
                    return $result;
                }
                // save input data if set
                if (isset ($image['input']['data'])) {
                    // Save the file
                    $origine_name = $image['input']['name'];
                    $file_type = pathinfo($origine_name, PATHINFO_EXTENSION);
                    $finalName = $name.".".$file_type;

                    $data = $image['input']['data'];
                    $input = Slim::saveFile($data, $finalName, $path, false);
                    array_push($files, $output);

                    $result = array('result' => 'success', 'image' => $files);
                    return $result;
                }
            }
        }
    }

    public function getSingleCategory($id)
    {
        $category = Category::find($id);
        if ($category) {
            $category_img_url = asset('uploads/categories/'.$category->image);
            $final_category = array();
            $final_category['category_id'] = $category->id;
            $final_category['category_name'] = $category->categoryName;
            $final_category['category_description'] = $category->categoryDescription;
            $final_category['category_position'] = $category->position;
            $final_category['category_img_url'] = $category_img_url;
            return response()->json(['result' => 'success', 'category' => $final_category]);
        }

        return response()->json(['result' => 'error']);
    }

    public function categoryUpdate(Request $request)
    {
        $edit_category = Category::find($request->category_id_edit);
        if ($edit_category) {
            $categories = Category::where('id', '!=', $request->category_id_edit)->orderBy('position', 'ASC')->get();
            if ($categories) {
                $position_number = $request->_category_position;
                foreach ($categories as $category) {
                    if ($category->position == $position_number) {
                        $category->position = $edit_category->position;
                        $category->save();
                    }
                }
            }

            $edit_category->categoryName = $request->_category_name;
            $edit_category->categoryDescription = $request->_category_description;
            $edit_category->position = $request->_category_position;
            $edit_category->save();

            if ($request->slim[0] != null) {
                $imagename = "category"."_".time();

                if(!is_dir(public_path('uploads/categories'))){
                    mkdir(public_path('uploads/categories'));
                }

                $dst = public_path('uploads/categories/');

                if (File::exists($dst . $edit_category->image)) {
                    File::delete($dst . $edit_category->image);
                }

                $finish_image = $this->uploadImagetoServer($request, $imagename, $dst);

                if ($finish_image['result'] == "success") {
                    $edit_category->image = $finish_image['image'][0]['name'];
                    $edit_category->save();
                }
            }

            return back()->with('success', 'category updated successfully');
        }

        return back()->with('error', 'Can not find Category');
    }

    public function categoryDestroy($id)
    {
        $del_category = Category::find($id);
        if ($del_category) {
            if(!is_dir(public_path('uploads/categories'))){
                mkdir(public_path('uploads/categories'));
            }

            $dst = public_path('uploads/categories/');

            if (File::exists($dst . $del_category->image)) {
                File::delete($dst . $del_category->image);
            }
            $del_category->delete();

            $categories = Category::orderBy('position', 'ASC')->get();
            if ($categories) {
                $position_number = 1;
                foreach ($categories as $category) {
                    $category->position = $position_number;
                    $category->save();
                    $position_number++;
                }
            }

            return back()->with('success', 'Category Deleted');
        }
        return back()->with('error', 'Can not find category');
    }

    public function productShow()
    {
        $products = Product::orderBy('id', 'DESC')->get();
        return view('product',['products' => $products]);
    }

    public function productAdd(Request $request)
    {
        $imagename = "product"."_".time();

        if(!is_dir(public_path('uploads/products'))){
            mkdir(public_path('uploads/products'));
        }

        $dst = public_path('uploads/products/');


        $finish_image = $this->uploadImagetoServer($request, $imagename, $dst);

        if ($finish_image['result'] == "success") {
            $price = (float) $request->product_price;
            $new_product = new Product;
            $new_product->cover_image = $finish_image['image'][0]['name'];
            $new_product->title = $request->product_title;
            $new_product->description = $request->product_description;
            $new_product->price =  $price;
            $new_product->save();

            return back()->with('success', 'New Product created successfully.');
        }
    }

    public function getSingleProduct($id)
    {
        $product = Product::find($id);
        if ($product) {
            $product_img_url = asset('uploads/products/'.$product->cover_image);
            $final_product = array();
            $final_product['product_id'] = $product->id;
            $final_product['product_title'] = $product->title;
            $final_product['product_description'] = $product->description;
            $final_product['product_price'] = $product->price;
            $final_product['product_img_url'] = $product_img_url;
            return response()->json(['result' => 'success', 'product' => $final_product]);
        }

        return response()->json(['result' => 'error']);
    }

    public function productUpdate(Request $request)
    {
        $edit_product = Product::find($request->product_id_edit);
        if ($edit_product) {
            $price = (float) $request->_product_price;

            $edit_product->title = $request->_product_title;
            $edit_product->description = $request->_product_description;
            $edit_product->price = $request->_product_price;
            $edit_product->save();

            if ($request->slim[0] != null) {
                $imagename = "product"."_".time();

                if(!is_dir(public_path('uploads/products'))){
                    mkdir(public_path('uploads/products'));
                }

                $dst = public_path('uploads/products/');

                if (File::exists($dst . $edit_product->cover_image)) {
                    File::delete($dst . $edit_product->cover_image);
                }

                $finish_image = $this->uploadImagetoServer($request, $imagename, $dst);

                if ($finish_image['result'] == "success") {
                    $edit_product->cover_image = $finish_image['image'][0]['name'];
                    $edit_product->save();
                }
            }

            return back()->with('success', 'Product updated successfully');
        }

        return back()->with('error', 'Can not find Product');
    }

    public function productDestroy($id)
    {
        $product = Product::find($id);
        if ($product) {
            $dst = public_path('uploads/products/');
            
            if (File::exists($dst . $product->cover_image)) {
                File::delete($dst . $product->cover_image);
            }
            $product->delete();
            return back()->with('success', 'Product Deleted');
        }
        return back()->with('error', 'Can not find Product');
    }

    public function extraShow()
    {
        $extras = Extra::orderBy('id', 'DESC')->get();
        return view('extra',['extras' => $extras]);
    }

    public function extraAdd(Request $request)
    {
        $price = (float) $request->extra_price;
        $new_extra = new Extra;
        $new_extra->name = $request->extra_name;
        $new_extra->price = $price;
        $new_extra->save();

        return back()->with('success', 'New Extra created successfully.');
    }

    public function getSingleExtra($id)
    {
        $extra = Extra::find($id);
        if ($extra) {
            $final_extra = array();
            $final_extra['extra_id'] = $extra->id;
            $final_extra['extra_name'] = $extra->name;
            $final_extra['extra_price'] = $extra->price;
            return response()->json(['result' => 'success', 'extra' => $final_extra]);
        }

        return response()->json(['result' => 'error']);
    }

    public function extraUpdate(Request $request)
    {
        $edit_extra = Extra::find($request->extra_id_edit);
        if ($edit_extra) {
            $price = (float) $request->_extra_price;
            $edit_extra->name = $request->_extra_name;
            $edit_extra->price = $price;
            $edit_extra->save();

            return back()->with('success', 'Extra updated successfully');
        }

        return back()->with('error', 'Can not find Extra');
    }

    public function extraDestroy($id)
    {
        $extra = Extra::find($id);
        if ($extra) {
            $extra->delete();
            return back()->with('success', 'Extra Deleted');
        }
        return back()->with('error', 'Can not find Extra');
    }
}
