<?php

namespace App\Http\Controllers;

use App\Slim;
use App\StoreInfo;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StoreInfoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $countries = Country::all();
        $storeinfo = StoreInfo::first();
        // dd($storeinfo->country_code);
        return view('storeinfo', ['countries' => $countries, 'storeinfo' => $storeinfo]);
    }

    public function updateAddress(Request $request)
    {
        $country = Country::where('code', $request->store_country)->first();

        $storeinfo = StoreInfo::first();
        $storeinfo->street = $request->store_street;
        $storeinfo->city = $request->store_city;
        $storeinfo->postalZip = $request->store_zip;
        $storeinfo->country = $country->name;
        $storeinfo->country_code = $country->code;
        $storeinfo->longitude = $request->coor_lng;
        $storeinfo->latitude = $request->coor_lat;
        $storeinfo->save();
        return back()->with('success', 'Store address Updated');
    }

    public function updateHeaderimg(Request $request)
    {
        $storeinfo = StoreInfo::first();
        $imagename = "header"."_".time();

        if(!is_dir(public_path('uploads/storeinfo'))){
            mkdir(public_path('uploads/storeinfo'));
        }

        $dst = public_path('uploads/storeinfo/');


        $finish_image = $this->uploadImagetoServer($request, $imagename, $dst);

        if ($finish_image['result'] == "success") {
            $storeinfo->header_img = $finish_image['image'][0]['name'];
            $storeinfo->save();

            return back()->with('success', 'Store Header image changed successfully.');
        }

        return back()->with('error', 'went something wrong');
    }

    public function updateBgimg(Request $request)
    {
        $storeinfo = StoreInfo::first();
        $imagename = "bg"."_".time();

        if(!is_dir(public_path('uploads/storeinfo'))){
            mkdir(public_path('uploads/storeinfo'));
        }

        $dst = public_path('uploads/storeinfo/');


        $finish_image = $this->uploadImagetoServer($request, $imagename, $dst);

        if ($finish_image['result'] == "success") {
            $storeinfo->bg_img = $finish_image['image'][0]['name'];
            $storeinfo->save();

            return back()->with('success', 'Store Header image changed successfully.');
        }

        return back()->with('error', 'went something wrong');
    }

    public function updateLogoimg(Request $request)
    {
        $storeinfo = StoreInfo::first();
        $imagename = "logo"."_".time();

        if(!is_dir(public_path('uploads/storeinfo'))){
            mkdir(public_path('uploads/storeinfo'));
        }

        $dst = public_path('uploads/storeinfo/');


        $finish_image = $this->uploadImagetoServer($request, $imagename, $dst);

        if ($finish_image['result'] == "success") {
            $storeinfo->logo_img = $finish_image['image'][0]['name'];
            $storeinfo->save();

            return back()->with('success', 'Store App logo image changed successfully.');
        }

        return back()->with('error', 'went something wrong');
    }

    public function uploadImagetoServer($imgdata, $name, $path)
    {
        $files = array();
        $result = array();
        $rules = [
            'file' => 'image',
            'slim[]' => 'image'
        ];

        $validator = Validator::make($imgdata->all(), $rules);
        $errors = $validator->errors();

        if($validator->fails()){
            $result = array('result' => 'fail', 'reson' => 'validator');
            return $result;
        }

        // Get posted data
        $images = Slim::getImages();

        // No image found under the supplied input name
        if ($images == false) {
            $result = array('result' => 'fail', 'reson' => 'image');
            return $result;
        } else {
            foreach ($images as $image) {
                // save output data if set
                if (isset($image['output']['data'])) {
                    // Save the file
                    $origine_name = $image['input']['name'];
                    $file_type = pathinfo($origine_name, PATHINFO_EXTENSION);
                    $finalName = $name.".".$file_type;

                    // We'll use the output crop data
                    $data = $image['output']['data'];
                    $output = Slim::saveFile($data, $finalName, $path, false);
                    array_push($files, $output);
                    $result = array('result' => 'success', 'image' => $files);
                    return $result;
                }
                // save input data if set
                if (isset ($image['input']['data'])) {
                    // Save the file
                    $origine_name = $image['input']['name'];
                    $file_type = pathinfo($origine_name, PATHINFO_EXTENSION);
                    $finalName = $name.".".$file_type;

                    $data = $image['input']['data'];
                    $input = Slim::saveFile($data, $finalName, $path, false);
                    array_push($files, $output);

                    $result = array('result' => 'success', 'image' => $files);
                    return $result;
                }
            }
        }
    }

    public function updateBasicinfo(Request $request)
    {
        $deliver_cost = (float) $request->store_delivery;
        $storeinfo = StoreInfo::first();
        $storeinfo->storeName = $request->store_name;
        $storeinfo->businessName = $request->store_bname;
        $storeinfo->phone = $request->store_phone;
        $storeinfo->deliveryCost = $deliver_cost;
        $storeinfo->save();

        return back()->with('success', 'Store Basic Infomation Updated');
    }

    public function updateDescription(Request $request)
    {
        $storeinfo = StoreInfo::first();
        $storeinfo->message = $request->store_message;
        $storeinfo->save();

        return back()->with('success', 'Store Basic Infomation Updated');
    }

    public function updateWeektime(Request $request)
    {
        $storeinfo = StoreInfo::first();
        $storeinfo->monday = $request->store_monday;
        $storeinfo->tuesday = $request->store_tuesday;
        $storeinfo->wednesday = $request->store_wednesday;
        $storeinfo->thursday = $request->store_thursday;
        $storeinfo->friday = $request->store_friday;
        $storeinfo->saturday = $request->store_saturday;
        $storeinfo->sunday = $request->store_sunday;
        $storeinfo->save();

        return back()->with('success', 'Store Week service time Updated');
    }
}
