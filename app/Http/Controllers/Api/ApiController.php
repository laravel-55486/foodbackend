<?php

namespace App\Http\Controllers\Api;

use File;
use DateTime;
use App\User;
use Validator;
use App\Cupon;
use App\PushLog;
use App\Product;
use App\Category;
use App\StoreInfo;
use App\DeviceBadge;
use App\ProductExtra;
use App\CategoryProduct;
use App\UserResetPassword;
use App\Mail\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Resources\ExtraResource;
use App\Http\Resources\CategoryResource;

class ApiController extends Controller
{

    public function SendResetCode(Request $request)
    {
        $input = $request->all();
        $user_email = $input['resetEmail'];

        $user = User::where('email', $user_email)->first();
        if ($user) {
            $code = rand(100000, 999999);
            Mail::to($user_email)->send(new PasswordReset($code));

            $delete_my_reset = UserResetPassword::where('user_id', $user->id)->delete();

            $new_reset = new UserResetPassword;
            $new_reset->user_id = $user->id;
            $new_reset->confirmCode = $code;
            $new_reset->save();

            return response()->json(['result' => 'success','user_id' => $user->id, 'sent_email' => $user_email, 'confirmCode' => $code], 200);
        }
        return response()->json(['result' => 'error','msg' => "We can't find a user with that e-mail address."], 401);
    }

    public function login(Request $request)
    {
        $getUserinfo = $this->credentials($request);
        $availablecheck = Auth::guard('user')->attempt($getUserinfo);
        if($availablecheck){
            $user = Auth::guard('user')->user();
            $device_array = unserialize($user->device_fcm);

            if (!in_array($request->device_fcm, $device_array)) {
                array_push($device_array, $request->device_fcm);
                $user->device_fcm = serialize($device_array);
                $user->save();
            }

            if($user->avatar == "default.png") {
                $user['avatar_url'] = asset('avatars/users/default.png');
            } else {
                $user['avatar_url'] = asset('avatars/users/'.$user->id.'/'.$user->avatar);
            }

            $success =  $user->createToken('MyApp')-> accessToken;
            return response()->json(['result' => 'success','token' => $success, 'user' => $user], 200);
        }
        else{
            return response()->json(['result' => 'error', 'message'=>'Credential not match'], 401);
        }
    }

    public function passwordreset(Request $request)
    {
        $input = $request->all();
        $user_id = $input['userId'];
        $confirm_code = $input['confirmCode'];

        $user = User::find($user_id);
        if ($user) {
            $check_reset_count = UserResetPassword::where('user_id', $user->id)->where('confirmCode', $confirm_code)->count();
            if ($check_reset_count == 0) {
                return response()->json(['result' => 'error','msg' => "this code Expired"], 401);
            }

            $user->password = bcrypt($input['newPassword']);
            $user->save();

            UserResetPassword::where('user_id', $user->id)->delete();

            $device_array = unserialize($user->device_fcm);

            if (!in_array($request->device_fcm, $device_array)) {
                array_push($device_array, $request->device_fcm);
                $user->device_fcm = serialize($device_array);
                $user->save();
            }

            if($user->avatar == "default.png") {
                $user['avatar_url'] = asset('avatars/users/default.png');
            } else {
                $user['avatar_url'] = asset('avatars/users/'.$user->id.'/'.$user->avatar);
            }

            $success =  $user->createToken('MyApp')-> accessToken;
            return response()->json(['result' => 'success','token' => $success, 'user' => $user], 200);
        }

        return response()->json(['result' => 'error','msg' => "Invalide Email"], 401);
    }

    public function validateLogin($request)
    {
        $this->validate($request, [
            'email' => 'required', 'password' => 'required'
        ]);
    }

    protected function credentials(Request $request)
    {
        return array_merge($request->only('email', 'password'));
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'gender' => 'required',
            'birth' => 'required',
            'password' => 'required',
            'device_fcm' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['result' => 'error', 'message'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['birth'] = $this->encode_date_format($input['birth']);
        $device_array = array();
        array_push($device_array, $input['device_fcm']);
        $input['device_fcm'] = serialize($device_array);
        $user = User::create($input);

        if($request->image) {
            $data = $request->image;

            $imageRand = time();
            $random_name = $user->id."_".$imageRand.".png";

            if(!is_dir(public_path('avatars/users/'.$user->id))){
                mkdir(public_path('avatars/users/'.$user->id));
            }

            $dst = public_path('avatars/users/'.$user->id.'/');

            if($user->avatar != 'default.png') {
                if (File::exists($dst . $user->avatar)) {
                    File::delete($dst . $user->avatar);
                }
            }

            $path = $dst.'/'.$random_name;

            $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));

            file_put_contents($path, $data);

            $user->avatar = $random_name;
            $user->save();

            $user['avatar_url'] = asset('avatars/users/'.$user->id.'/'.$user->avatar);
        } else {
            $user['avatar_url'] = asset('avatars/users/default.png');
        }
        // return $user;
        $success =  $user->createToken('MyApp')-> accessToken;
        return response()->json(['result' => 'success','token' => $success, 'user' => $user], 200);
    }

    public function details()
    {
        $user = Auth::user();
        if($user->avatar == "default.png") {
            $user['avatar_url'] = asset('avatars/users/default.png');
        } else {
            $user['avatar_url'] = asset('avatars/users/'.$user->id.'/'.$user->avatar);
        }
        return response()->json(['result' => 'success' ,'user' => $user], 200);
    }

    public function decode_date_format($date)
    {
        $selectedDate = DateTime::createFromFormat('Y-m-d H:i:s', $date);
        $finalDate = $selectedDate->format('d/m/Y');
        return $finalDate;
    }

    public function encode_date_format($date)
    {
        $selectedDate = DateTime::createFromFormat('d/m/Y', $date);
        $finalDate = $selectedDate->format('Y-m-d');
        return $finalDate;
    }

    public function decode_time_format($time)
    {
        $selectedDate = DateTime::createFromFormat('Y-m-d H:i:s', $time);
        $finalTime = $selectedDate->format('H:i');
        return $finalTime;
    }

    public function getPushHistory()
    {
        $user = Auth::user();
        $histories = PushLog::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();
        $total_date_array = array();
        $final_array = array();
        $notification_array = array();
        $date = "";
        foreach ($histories as $history) {
            $current_date = $this->decode_date_format($history->created_at);;
            if($date != $current_date) {
                array_push($total_date_array, $current_date);
                $date = $current_date;
            }
        }

        foreach ($total_date_array as $singledate) {
            $final_sub_array = array();
            foreach ($histories as $history) {
                $current_date = $this->decode_date_format($history->created_at);
                $current_time = $this->decode_time_format($history->created_at);
                if($singledate == $current_date) {
                    $final_sub_array[] = array(
                        'msg_title' => $history->msg_title,
                        'msg_text' => $history->msg_text,
                        'msg_time' => $current_time,
                    );
                }
            }

            $final_array[] = array(
                'date' => $singledate,
                'data' => $final_sub_array
            );
        }

        $budge_number = 0;
        $current_badge_count = DeviceBadge::where('user_id', $user->id)->count();
        if ($current_badge_count > 0) {
            $current_badge = DeviceBadge::where('user_id', $user->id)->first();

            $budge_number = $current_badge->badge_num;
        }

        $notification_array['badge_num'] = $budge_number;
        $notification_array['notification'] = $final_array;

        return response()->json(['data' => $notification_array], 200);
    }

    public function setPushHistoryBadge()
    {
        $user = Auth::user();
        $histories = PushLog::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();
        $total_date_array = array();
        $final_array = array();
        $notification_array = array();
        $date = "";
        foreach ($histories as $history) {
            $current_date = $this->decode_date_format($history->created_at);;
            if($date != $current_date) {
                array_push($total_date_array, $current_date);
                $date = $current_date;
            }
        }

        foreach ($total_date_array as $singledate) {
            $final_sub_array = array();
            foreach ($histories as $history) {
                $current_date = $this->decode_date_format($history->created_at);
                $current_time = $this->decode_time_format($history->created_at);
                if($singledate == $current_date) {
                    $final_sub_array[] = array(
                        'msg_title' => $history->msg_title,
                        'msg_text' => $history->msg_text,
                        'msg_time' => $current_time,
                    );
                }
            }

            $final_array[] = array(
                'date' => $singledate,
                'data' => $final_sub_array
            );
        }

        $budge_number = 0;
        $current_badge = DeviceBadge::where('user_id', $user->id)->first();
        if ($current_badge) {
            $current_badge->badge_num = 0;
            $current_badge->save();
        }

        $notification_array['badge_num'] = $budge_number;
        $notification_array['notification'] = $final_array;

        return response()->json(['data' => $notification_array], 200);
    }

    public function clearBadge()
    {
        $user = Auth::user();
        $current_badge = DeviceBadge::where('user_id', $user->id)->first();
        if ($current_badge) {
            $current_badge->badge_num = 0;
            $current_badge->save();
            return response()->json(['result' => 'success'], 200);
        }
        return response()->json(['result' => 'error'], 200);
    }

    public function getBadgeNumber()
    {
        $user = Auth::user();
        $current_badge = DeviceBadge::where('user_id', $user->id)->first();
        if ($current_badge) {
            if($current_badge->badge_num > 0) {
                return response()->json(['result' => 'success', 'number' => $current_badge->badge_num], 200);
            }
        }
        return response()->json(['result' => 'error'], 200);
    }

    public function cuponCheck($couponString)
    {
        $user = Auth::user();
        $couponCount = Cupon::where('discountName', $couponString)->where('user_id', $user->id)->count();
        if($couponCount > 0){
            $coupon = Cupon::where('discountName', $couponString)->where('user_id', $user->id)->first();
            return response()->json(['result' => 'success', 'cupon' => $coupon], 200);
        }else{
            return response()->json(['result' => 'null'], 200);
        }

    }

    public function logoImg()
    {
        $storeinfo = StoreInfo::first();
        $logoImgUrl = asset('uploads/storeinfo/'.$storeinfo->logo_img);
        return response()->json($logoImgUrl);
    }

    public function headerImg()
    {
        $storeinfo = StoreInfo::first();
        $headerImgUrl = asset('uploads/storeinfo/'.$storeinfo->header_img);
        return response()->json($headerImgUrl);
    }

    public function getCatalogs($id, $extras = null)
    {
        $categories = Category::orderBy('position', 'ASC')->get();
        $all_foods = CategoryResource::collection($categories);
        return response()->json($all_foods);
    }

    public function getExtrasProduct($id)
    {
        $product = Product::find($id);
        return ExtraResource::collection($product->extras)->sortBy('position');
    }

    public function storeInfo()
    {
        $storeinfo = StoreInfo::first();
        return response()->json(['data' => $storeinfo]);
    }
}
