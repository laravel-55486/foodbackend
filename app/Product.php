<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function extras(){
        return $this->belongsToMany('App\Extra', 'product_extras')->withPivot('position');
    }

}
