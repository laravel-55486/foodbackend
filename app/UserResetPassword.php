<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserResetPassword extends Model
{
    protected $table = 'user_reset_passwords';
}
