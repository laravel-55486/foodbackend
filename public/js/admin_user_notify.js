var extra_table = $('#user_notify_table').dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "Notification not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "Notification not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "width": "5%",
        },
        {
            "targets": 1,
            "width": "45%",
        },
        {
            "targets": 2,
            "width": "50%",
        },
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 10,
    "order": [
        [0, "asc"]
    ]
});
