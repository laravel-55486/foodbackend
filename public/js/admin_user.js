var extra_table = $('#user_table').dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "User not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "User not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "width": "3%",
        },
        {
            "targets": 1,
            "width": "30%",
        },
        {
            "targets": 2,
            "width": "47%",
        },
        {
            "targets": 3,
            "orderable": false,
            "searchable": false,
            "width": "20%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 5,
    "order": [
        [0, "desc"]
    ]
});

$(document).on('click', '.user_delete_btn', function() {
    var $user_id = $(this).data('user_id');
    swal({
        title: "Are you sure?",
        text: "Do you want to delete this extra",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55  ",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }, function () {
        var final_delete_url = '/user/delete/'+$user_id;
        window.location.replace(final_delete_url);
    });
});

$(document).on('click', '.user_notify_btn', function() {
    var $user_id = $(this).data('user_id');
    $('#new_notify_form #notify_user_id').val($user_id);
    $('#new_notify_modal').modal('show');
});

$(document).on('click', '.user_detail_btn', function() {
    var $user_id = $(this).data('user_id');
    var user_detail_url = '/user/detail/'+$user_id;
    window.location.replace(user_detail_url);
});
