var category_table = $('#product_table').dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "Product not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "Product not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "width": "3%",
        },
        {
            "targets": 1,
            "orderable": false,
            "searchable": false,
            "width": "10%",
        },
        {
            "targets": 2,
            "width": "23%",
        },
        {
            "targets": 3,
            "width": "40%",
        },
        {
            "targets": 4,
            "width": "5%",
        },
        {
            "targets": 5,
            "orderable": false,
            "searchable": false,
            "width": "19%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 5,
    "order": [
        [0, "desc"]
    ]
});

var product_img_cropper = new Slim(document.getElementById('product_header_img'), {
    ratio: '1:1',
    minSize: {
        width: 100,
        height: 100,
    },
    download: false,
    label: 'Drop your image here or Click.',
    statusImageTooSmall:'Image too small. Min Size is $0 pixel. Try again',
});
product_img_cropper.size = { width:1000, height:1000 };

var edit_product_img_cropper = new Slim(document.getElementById('edit_product_header_img'), {
    ratio: '1:1',
    minSize: {
        width: 100,
        height: 100,
    },
    download: false,
    label: 'Drop your image here or Click.',
    statusImageTooSmall:'Image too small. Min Size is $0 pixel. Try again',
});
edit_product_img_cropper.size = { width:1000, height:1000 };

$(document).on('click', '.product_edit_btn', function() {
    $product_id = $(this).data('product_id');
    var final_url = '/product/getsingle/'+$product_id;
    $.ajax({
        url: final_url,
        type: 'get',
        success: function(response){
            if (response.result == "success") {
                edit_product_img_cropper.destroy();
                var edit_form = $('#edit_product_form');
                edit_form.find('#edit_product_header_img>img').attr('src', response.product.product_img_url);

                edit_product_img_cropper = new Slim(document.getElementById('edit_product_header_img'), {
                    ratio: '1:1',
                    minSize: {
                        width: 100,
                        height: 100,
                    },
                    download: false,
                    label: 'Drop your image here or Click.',
                    statusImageTooSmall:'Image too small. Min Size is $0 pixel. Try again',
                });
                edit_product_img_cropper.size = { width:1000, height:1000 };
                edit_form.find('#product_id_edit').val(response.product.product_id);
                edit_form.find('#_product_title').val(response.product.product_title);
                edit_form.find('#_product_price').val(response.product.product_price);
                edit_form.find('#_product_description').val(response.product.product_description);
                $('#edit_product_modal').modal('show');
            }
        },
        error: function(error){
            console.log(error);
        }
    });
})

$(document).on('click', '.product_delete_btn', function() {
    $product_id = $(this).data('product_id');
    swal({
        title: "Are you sure?",
        text: "Do you want to delete this product",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55  ",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }, function () {
        var final_delete_url = '/product/delete/'+$product_id;
        window.location.replace(final_delete_url);
    });
})

$('.product_price_box').on('keypress', function(e) {
    var $this = $(this);
    var available_keys = [44,45,46,48,49,50,51,52,53,54,55,56,57];
    var current_code = parseInt(e.which);
    if ($.inArray(current_code, available_keys) > -1) {

    } else {
        return false;
    }
});

$('.product_price_box').on('keyup', function(e) {
    var $this = $(this);
    if (e.keyCode == 188) {
        var current_string = $this.val();
        current_string = current_string.replace(/,/g , ".");
        $this.val(current_string);
    }
});
