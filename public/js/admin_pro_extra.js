var product_extra_table = $('#product_extra_table').dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "Product extra not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "Product extra not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "width": "50%",
        },
        {
            "targets": 1,
            "width": "10%",
        },
        {
            "targets": 2,
            "width": "20%",
        },
        {
            "targets": 3,
            "orderable": false,
            "searchable": false,
            "width": "20%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 5,
    "order": [
        [2, "asc"]
    ]
});

var add_extra_table  = $('#add_extra_table').dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "Extra not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "Extra not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "searchable": false,
            "orderable": false,
            "width": "10%",
        },
        {
            "targets": 1,
            "searchable": false,
            "width": "10%",
        },
        {
            "targets": 2,
            "width": "30%",
        },
        {
            "targets": 3,
            "width": "50%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 5,
    "order": [
        [1, "desc"]
    ]
});

$('#pro_add_extra_form').on('submit', function(e) {
    e.preventDefault();
    var $this = $(this);
    var submitalble = false;
    $this.find('input[type=checkbox].square-blue').each(function() {
        $that = $(this);
        if ($that.is(":checked")) {
            submitalble = true;
        }
    });

    if (submitalble) {
        $this[0].submit();
    }
});

$('.pro_extra_position_select').on('change', function() {
    var selected_position = this.value;
    var pro_extra_id = $(this).data('pro_extra_id');
    var pro_origin_position = $(this).data('origin_position');
    if (selected_position != pro_origin_position) {
        console.log("will submit");
        var change_url = "/product/extra/change/position/"+pro_extra_id+"/"+selected_position;
        window.location.replace(change_url);
    }
})

$('input[type="checkbox"].square-blue').iCheck({
    checkboxClass: 'icheckbox_square-blue'
});

$(document).on('click', '.pro_extra_delete_btn', function() {
    $pro_extra_id = $(this).data('pro_extra_id');
    swal({
        title: "Are you sure?",
        text: "Do you want to delete this Extra?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55  ",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }, function () {
        var final_delete_url = '/product/extra/delete/'+$pro_extra_id;
        window.location.replace(final_delete_url);
    });
})

$('#product_extra_sort_form').on('submit', function(e) {
    e.preventDefault();
    // console.log(ddd);
    var order = localStorage.getItem(local_pro_extra_sortable);
    var splited_order = order ? order.split('|') : [];
    var order_url = "/product/extra/setOrder";
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        }
    });

    $.post(
        order_url,
        {'sort_list': splited_order},
        function(data, status){
            console.log(status);
            if (status == "success") {
                location.reload();
            }
        }
    );
});
