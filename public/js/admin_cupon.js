var App = function() {
    var handleUniform = function() {
        if (!$().uniform) {
            return;
        }
        var test = $("input[type=checkbox]:not(.toggle, .md-check, .md-radiobtn, .make-switch, .icheck), input[type=radio]:not(.toggle, .md-check, .md-radiobtn, .star, .make-switch, .icheck)");
        if (test.size() > 0) {
            test.each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });
        }
    };
    return {

        //main function to initiate the theme
        init: function() {
            handleUniform();
        }
    }
}();
var cupon_table = $('#cupon_table').dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "Cupon not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "Cupon not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "width": "3%",
        },
        {
            "targets": 1,
            "width": "30%",
        },
        {
            "targets": 2,
            "width": "30%",
        },
        {
            "targets": 3,
            "width": "20%",
        },
        {
            "targets": 4,
            "orderable": false,
            "searchable": false,
            "width": "17%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 5,
    "order": [
        [0, "desc"]
    ]
});

var cupon_user_table = $('#add_cupon_user_table');
cupon_user_table.dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "User not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "User not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "orderable": false,
            "searchable": false,
            "width": "3%",
        },
        {
            "targets": 1,
            "width": "30%",
        },
        {
            "targets": 2,
            "width": "30%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 5,
    "order": [
        [1, "asc"]
    ]
});

cupon_user_table.find('.group-checkable').change(function () {
    var set = jQuery(this).attr("data-set");
    var checked = jQuery(this).is(":checked");
    jQuery(set).each(function () {
        if (checked) {
            $(this).prop("checked", true);
        } else {
            $(this).prop("checked", false);
        }
    });
    jQuery.uniform.update(set);
});

$("#cupon_user").select2({
    theme: "bootstrap",
    placeholder: "Select User"
});

$(document).on('click', '.cupon_edit_btn', function() {
    $cupon_id = $(this).data('cupon_id');
    var final_url = '/cupon/getsingle/'+$cupon_id;
    $.ajax({
        url: final_url,
        type: 'get',
        success: function(response){
            if (response.result == "success") {
                var edit_form = $('#edit_cupon_form');
                edit_form.find('#cupon_id_edit').val(response.cupon.id);
                edit_form.find('#_cupon_name').val(response.cupon.discountName);
                edit_form.find('#_cupon_percent').val(response.cupon.percentage);
                edit_form.find('#_cupon_user').val(response.cupon.user_id);
                edit_form.find('#_cupon_user').select2({
                    theme: "bootstrap",
                    placeholder: "Select User"
                });
                $('#edit_cupon_modal').modal('show');
            }
        },
        error: function(error){
            console.log(error);
        }
    });
})

$(document).on('click', '.cupon_delete_btn', function() {
    $cupon_id = $(this).data('cupon_id');
    swal({
        title: "Are you sure?",
        text: "Do you want to delete this Cupon ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55  ",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }, function () {
        var final_delete_url = '/cupon/delete/'+$cupon_id;
        window.location.replace(final_delete_url);
    });
})

$('.cupon_percent_box').on('keypress', function(e) {
    var $this = $(this);
    var available_keys = [44,45,46,48,49,50,51,52,53,54,55,56,57];
    var current_code = parseInt(e.which);
    if ($.inArray(current_code, available_keys) > -1) {

    } else {
        return false;
    }
});

$('.cupon_percent_box').on('keyup', function(e) {
    var $this = $(this);
    if (e.keyCode == 188) {
        var current_string = $this.val();
        current_string = current_string.replace(/,/g , ".");
        $this.val(current_string);
    }
});

jQuery(document).ready(function() {
   App.init(); // init metronic core componets
});
