var category_product_table = $('#category_product_table').dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "Category Product not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "Category Product not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "orderable": false,
            "searchable": false,
            "width": "8%",
        },
        {
            "targets": 1,
            "width": "25%",
        },
        {
            "targets": 2,
            "width": "42%",
        },
        {
            "targets": 3,
            "width": "5%",
        },
        {
            "targets": 4,
            "width": "10%",
        },
        {
            "targets": 5,
            "orderable": false,
            "searchable": false,
            "width": "10%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 5,
    "order": [
        [4, "asc"]
    ]
});

var category_table = $('#category_table').dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "Category not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "Category not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "width": "3%",
        },
        {
            "targets": 1,
            "orderable": false,
            "searchable": false,
            "width": "20%",
        },
        {
            "targets": 2,
            "width": "23%",
        },
        {
            "targets": 3,
            "width": "30%",
        },
        {
            "targets": 4,
            "width": "5%",
        },
        {
            "targets": 5,
            "orderable": false,
            "searchable": false,
            "width": "19%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 5,
    "order": [
        [0, "desc"]
    ]
});

var add_product_table = $('#add_product_table');

add_product_table.dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "Product not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "Product not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "searchable": false,
            "orderable": false,
            "width": "5%",
        },
        {
            "targets": 1,
            "searchable": false,
            "width": "5%",
        },
        {
            "targets": 2,
            "width": "30%",
        },
        {
            "targets": 3,
            "width": "50%",
        },
        {
            "targets": 4,
            "width": "10%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 15,
    "order": [
        [1, "desc"]
    ]
});

$('#cat_add_product_form').on('submit', function(e) {
    e.preventDefault();
    var $this = $(this);
    var submitalble = false;
    $this.find('input[type=checkbox].square-blue').each(function() {
        $that = $(this);
        if ($that.is(":checked")) {
            submitalble = true;
        }
    });

    if (submitalble) {
        $this[0].submit();
    }
});

$('.cat_product_position_select').on('change', function() {
    var selected_position = this.value;
    var cat_product_id = $(this).data('cat_product_id');
    var cat_origin_position = $(this).data('origin_position');
    if (selected_position != cat_origin_position) {
        console.log("will submit");
        var change_url = "/category/product/change/position/"+cat_product_id+"/"+selected_position;
        window.location.replace(change_url);
    }
})

$('input[type="checkbox"].square-blue').iCheck({
    checkboxClass: 'icheckbox_square-blue'
});

$(document).on('click', '.cat_product_delete_btn', function() {
    $cat_product_id = $(this).data('cat_product_id');
    swal({
        title: "Are you sure?",
        text: "Do you want to delete this product",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55  ",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }, function () {
        var final_delete_url = '/category/product/delete/'+$cat_product_id;
        window.location.replace(final_delete_url);
    });
})

$('#category_product_sort_form').on('submit', function(e) {
    e.preventDefault();
    var order = localStorage.getItem(local_cat_product_sortable);
    var splited_order = order ? order.split('|') : [];
    var order_url = "/category/product/setOrder";
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        }
    });

    $.post(
        order_url,
        {'sort_list': splited_order},
        function(data, status){
            console.log(status);
            if (status == "success") {
                location.reload();
            }
        }
    );
});
