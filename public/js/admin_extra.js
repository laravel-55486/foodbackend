var extra_table = $('#extra_table').dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "Extra not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "Extra not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "width": "3%",
        },
        {
            "targets": 1,
            "width": "30%",
        },
        {
            "targets": 2,
            "width": "50%",
        },
        {
            "targets": 3,
            "orderable": false,
            "searchable": false,
            "width": "17%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 5,
    "order": [
        [0, "desc"]
    ]
});

$(document).on('click', '.extra_edit_btn', function() {
    $extra_id = $(this).data('extra_id');
    var final_url = '/extra/getsingle/'+$extra_id;
    $.ajax({
        url: final_url,
        type: 'get',
        success: function(response){
            if (response.result == "success") {
                var edit_form = $('#edit_extra_form');
                edit_form.find('#extra_id_edit').val(response.extra.extra_id);
                edit_form.find('#_extra_name').val(response.extra.extra_name);
                edit_form.find('#_extra_price').val(response.extra.extra_price);
                $('#edit_extra_modal').modal('show');
            }
        },
        error: function(error){
            console.log(error);
        }
    });
})

$(document).on('click', '.extra_delete_btn', function() {
    $extra_id = $(this).data('extra_id');
    swal({
        title: "Are you sure?",
        text: "Do you want to delete this extra",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55  ",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }, function () {
        var final_delete_url = '/extra/delete/'+$extra_id;
        window.location.replace(final_delete_url);
    });
})

$('.extra_price_box').on('keypress', function(e) {
    var $this = $(this);
    var available_keys = [44,45,46,48,49,50,51,52,53,54,55,56,57];
    var current_code = parseInt(e.which);
    if ($.inArray(current_code, available_keys) > -1) {

    } else {
        return false;
    }
});

$('.extra_price_box').on('keyup', function(e) {
    var $this = $(this);
    if (e.keyCode == 188) {
        var current_string = $this.val();
        current_string = current_string.replace(/,/g , ".");
        $this.val(current_string);
    }
});
