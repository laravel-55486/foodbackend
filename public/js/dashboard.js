var options = {
        useEasing : true, // toggle easing
        useGrouping : false, // 1,000,000 vs 1000000
        separator : ',', // character to use as a separator
        decimal : '.' // character to use as a decimal
    };

var today_view = $('#user_view_day').data('value');
var week_view = $('#user_view_week').data('value');
var month_view = $('#user_view_month').data('value');

var today_order = $('#user_order_day').data('value');
var week_order = $('#user_order_week').data('value');
var month_order = $('#user_order_month').data('value');

var today_cash = $('#user_cash_day').data('value');
var week_cash = $('#user_cash_week').data('value');
var month_cash = $('#user_cash_month').data('value');

var today_register = $('#user_register_day').data('value');
var week_register = $('#user_register_week').data('value');
var month_register = $('#user_register_month').data('value');

//view
var demo = new CountUp("user_view_day", 0, today_view, 0, 6, options);
demo.start();
var demo = new CountUp("user_view_week", 0, week_view, 0, 6, options);
demo.start();
var demo = new CountUp("user_view_month", 0, month_view, 0, 6, options);
demo.start();
//order
var demo = new CountUp("user_order_day", 0, today_order, 0, 6, options);
demo.start();
var demo = new CountUp("user_order_week", 0, week_order, 0, 6, options);
demo.start();
var demo = new CountUp("user_order_month", 0, month_order, 0, 6, options);
demo.start();
//cash
var demo = new CountUp("user_cash_day", 0, today_cash, 0, 6, options);
demo.start();
var demo = new CountUp("user_cash_week", 0, week_cash, 0, 6, options);
demo.start();
var demo = new CountUp("user_cash_month", 0, month_cash, 0, 6, options);
demo.start();
//register
var demo = new CountUp("user_register_day", 0, today_register, 0, 6, options);
demo.start();
var demo = new CountUp("user_register_week", 0, week_register, 0, 6, options);
demo.start();
var demo = new CountUp("user_register_month", 0, month_register, 0, 6, options);
demo.start();

$('.dashboard-tap-btn').on('click', function() {
    var $this = $(this);
    $('#chart-loading-view').fadeIn('fast');
    $this.parent().find('.dashboard-tap-btn').each(function(e) {
        $(this).removeClass('active');
    });

    $this.addClass('active');
    setTimeout(function(){
        drawDay();
        drawWeek();
        drawMonth();
        drawYear();
    }, 500);

    setTimeout(function(){
        $('#chart-loading-view').fadeOut('slow');
    }, 700);

})
