var category_table = $('#category_table').dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "Category not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "Category not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "width": "3%",
        },
        {
            "targets": 1,
            "orderable": false,
            "searchable": false,
            "width": "20%",
        },
        {
            "targets": 2,
            "width": "23%",
        },
        {
            "targets": 3,
            "width": "30%",
        },
        {
            "targets": 4,
            "width": "5%",
        },
        {
            "targets": 5,
            "orderable": false,
            "searchable": false,
            "width": "19%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 5,
    "order": [
        [4, "asc"]
    ]
});

$("#category_position").select2({
    theme: "bootstrap",
    placeholder: "Select category position"
});

var category_img_cropper = new Slim(document.getElementById('category_header_img'), {
    ratio: '5:1',
    minSize: {
        width: 250,
        height: 50,
    },
    download: false,
    label: 'Drop your image here or Click.',
    statusImageTooSmall:'Image too small. Min Size is $0 pixel. Try again',
});
category_img_cropper.size = { width:1000, height:200 };

var edit_category_img_cropper = new Slim(document.getElementById('edit_category_header_img'), {
    ratio: '5:1',
    minSize: {
        width: 250,
        height: 50,
    },
    download: false,
    label: 'Drop your image here or Click.',
    statusImageTooSmall:'Image too small. Min Size is $0 pixel. Try again',
});
edit_category_img_cropper.size = { width:1000, height:200 };

$(document).on('click', '.category_edit_btn', function() {
    $category_id = $(this).data('category_id');
    var final_url = '/category/getsingle/'+$category_id;
    $.ajax({
        url: final_url,
        type: 'get',
        success: function(response){
            if (response.result == "success") {
                edit_category_img_cropper.destroy();
                var edit_form = $('#edit_category_add_form');
                edit_form.find('#edit_category_header_img>img').attr('src', response.category.category_img_url);

                edit_category_img_cropper = new Slim(document.getElementById('edit_category_header_img'), {
                    ratio: '5:1',
                    minSize: {
                        width: 250,
                        height: 50,
                    },
                    download: false,
                    label: 'Drop your image here or Click.',
                    statusImageTooSmall:'Image too small. Min Size is $0 pixel. Try again',
                });
                edit_category_img_cropper.size = { width:1000, height:200 };
                edit_form.find('#category_id_edit').val(response.category.category_id);
                edit_form.find('#_category_name').val(response.category.category_name);
                edit_form.find('#_category_position').val(response.category.category_position);
                edit_form.find('#_category_position').select2({
                    theme: "bootstrap",
                    placeholder: "Select User"
                });
                edit_form.find('#_category_description').val(response.category.category_description);
                $('#edit_category_modal').modal('show');
            }
        },
        error: function(error){
            console.log(error);
        }
    });
})

$(document).on('click', '.category_delete_btn', function() {
    $category_id = $(this).data('category_id');
    swal({
        title: "Are you sure?",
        text: "Do you want to delete this category",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55  ",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }, function () {
        var final_delete_url = '/category/delete/'+$category_id;
        window.location.replace(final_delete_url);
    });
})

$('#category_sort_form').on('submit', function(e) {
    e.preventDefault();
    var order = localStorage.getItem(local_category_sortable);
    var splited_order = order ? order.split('|') : [];
    var order_url = "/category/setOrder";
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        }
    });

    $.post(
        order_url,
        {'sort_list': splited_order},
        function(data, status){
            console.log(status);
            if (status == "success") {
                location.reload();
            }
        }
    );
});
