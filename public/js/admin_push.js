var App = function() {
    var handleUniform = function() {
        if (!$().uniform) {
            return;
        }
        var test = $("input[type=checkbox]:not(.toggle, .md-check, .md-radiobtn, .make-switch, .icheck), input[type=radio]:not(.toggle, .md-check, .md-radiobtn, .star, .make-switch, .icheck)");
        if (test.size() > 0) {
            test.each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });
        }
    };
    return {

        //main function to initiate the theme
        init: function() {
            handleUniform();
        }
    }
}();

var extra_table = $('#push_table').dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "User not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "User not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "width": "3%",
        },
        {
            "targets": 1,
            "width": "15%",
        },
        {
            "targets": 2,
            "width": "20%",
        },
        {
            "targets": 3,
            "width": "42%",
        },
        {
            "targets": 4,
            "width": "20%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 10,
    "order": [
        [0, "desc"]
    ]
});

var user_table = $('#notify_user_table');
user_table.dataTable({
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "User not found",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "User not found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        }
    },
    responsive:true,
    "columnDefs": [
        {
            "targets": 0,
            "orderable": false,
            "searchable": false,
            "width": "3%",
        },
        {
            "targets": 1,
            "width": "30%",
        },
        {
            "targets": 2,
            "width": "30%",
        }
    ],

    "lengthMenu": [
        [5, 10, 15, 25, -1],
        [5, 10, 15, 25, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 5,
    "order": [
        [1, "asc"]
    ]
});

user_table.find('.group-checkable').change(function () {
    var set = jQuery(this).attr("data-set");
    var checked = jQuery(this).is(":checked");
    jQuery(set).each(function () {
        if (checked) {
            $(this).prop("checked", true);
        } else {
            $(this).prop("checked", false);
        }
    });
    jQuery.uniform.update(set);
});

$(document).on('click', '.user_delete_btn', function() {
    var $user_id = $(this).data('user_id');
    swal({
        title: "Are you sure?",
        text: "Do you want to delete this extra",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55  ",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }, function () {
        var final_delete_url = '/user/delete/'+$user_id;
        window.location.replace(final_delete_url);
    });
});

$(document).on('click', '.user_notify_btn', function() {
    var $user_id = $(this).data('user_id');
    $('#new_notify_form #notify_user_id').val($user_id);
    $('#new_notify_modal').modal('show');
});

$(document).on('click', '.user_detail_btn', function() {
    var $user_id = $(this).data('user_id');
    var user_detail_url = '/user/detail/'+$user_id;
    window.location.replace(user_detail_url);
});

$('#new_push_send_form').on('submit', function(e) {
    e.preventDefault();
    var $this = $(this);
    var submitalble = false;
    $this.find('input[type=checkbox].checkboxes').each(function() {
        $that = $(this);
        if ($that.is(":checked")) {
            submitalble = true;
        }
    });

    if (submitalble) {
        $this[0].submit();
    } else {
        alert('select user');
    }
})

jQuery(document).ready(function() {
   App.init(); // init metronic core componets
});
