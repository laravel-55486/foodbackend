
     $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.editable').editable();

        $('#admin_password_change_form').submit(function(e) {
            e.preventDefault();
            var form = $(this)[0];
            var new_pass = $('#inputpassword').val();
            var new_c_pass = $('#inputnumber').val();
            if (new_pass != "" && new_pass == new_c_pass) {
                if (new_pass.length > 5) {
                    form.submit();
                }
            }
        });

        $("input[type=password]").keyup(function(){
            var ucase = new RegExp("[A-Z]+");
            var lcase = new RegExp("[a-z]+");
            var num = new RegExp("[0-9]+");

            if($("#inputpassword").val().length >= 6){
                $("#6char").removeClass("glyphicon-remove");
                $("#6char").addClass("glyphicon-ok");
                $("#6char").css("color","#00A41E");
            }else{
                $("#6char").removeClass("glyphicon-ok");
                $("#6char").addClass("glyphicon-remove");
                $("#6char").css("color","#FF0004");
            }

            if ($("#inputpassword").val() == $("#inputnumber").val()) {
                $("#pwmatch").removeClass("glyphicon-remove");
                $("#pwmatch").addClass("glyphicon-ok");
                $("#pwmatch").css("color","#00A41E");
            }else{
                $("#pwmatch").removeClass("glyphicon-ok");
                $("#pwmatch").addClass("glyphicon-remove");
                $("#pwmatch").css("color","#FF0004");
            }
            if ($("#inputpassword").val() =="" && $("#inputnumber").val()=="") {
                $("#pwmatch").removeClass("glyphicon-ok");
                $("#pwmatch").addClass("glyphicon-remove");
                $("#pwmatch").css("color","#FF0004");
            }
        });
});
